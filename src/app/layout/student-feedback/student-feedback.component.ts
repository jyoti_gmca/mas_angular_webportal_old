import { Component, OnInit, ViewChild } from '@angular/core';
import { StudentFeedbackService } from '../../Services/student-feedback.service';
import { MatPaginator, MatSort, MatTableDataSource, MatDialog } from '@angular/material';
import { StudentFeedBack } from '../../Model/student-feedback-model';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-student-feedback',
  templateUrl: './student-feedback.component.html',
  styleUrls: ['./student-feedback.component.scss']
})
export class StudentFeedbackComponent implements OnInit {
  studentFeedbackList = [];
  dataSource: MatTableDataSource<StudentFeedBack>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  formGroup: FormGroup;
  userRole;
  instituteId;
  departmentId;
  userId;
  facultyList;

  searchObj = {
    facultyId: "",
    startDate:"",
    endDate :""
  }

  pagination ={
    "page":0,
    "size":5,
    "total":0,
    "currentPage":0
  }

  constructor(
    public dialog: MatDialog,
    private studentFeedbackService: StudentFeedbackService,
    private _formBuilder: FormBuilder,
  ) {
    this.formGroup = this._formBuilder.group({
      facultyId: ['', []],
      startDate: ['', []],
      endDate: ['', []]
    });
  }

  ngOnInit() {
    this.userRole = localStorage.getItem('userRole');
    this.userId = localStorage.getItem('userId');
    this.getList(1);
    // this.dataSource.paginator = this.paginator;
    // this.dataSource.sort = this.sort;

    this.instituteId = localStorage.getItem('instituteId');
    this.departmentId = localStorage.getItem('departmentId');

    this.getFacultyDropdown();
  }

  getFacultyDropdown() {
    // console.log("instituteId :: ", this.instituteId);
    this.studentFeedbackService.getFacultyListByInstituteId(this.instituteId)
      .subscribe(res => {
        // console.log("Faculty List ::", res);
        this.facultyList = res;
      }, err => {
        //this._popupService.showError(err.message);
      });
  }

  search(formValue, formValid) {
    if (formValid) {
      this.getList(1);
    }
  }

  getList(page: number) {
    var olderPageValue = this.pagination.page;
    this.pagination.page = page - 1;

    let studentFeedbackRequest = {

      "facultyId": "",
      "startDate":"",
      "endDate":"",
      "page": 0,
      "limit": 10
    }

    if (this.searchObj.facultyId !== "") {
      studentFeedbackRequest.facultyId = this.searchObj.facultyId;
    }

    if (this.searchObj.startDate != "") {
      studentFeedbackRequest.startDate = this.searchObj.startDate;
    } else {
      studentFeedbackRequest.startDate = "";
    }

    if (this.searchObj.endDate !== "") {
      studentFeedbackRequest.endDate = this.searchObj.endDate;
    } else {
      studentFeedbackRequest.endDate = "";
    }

    studentFeedbackRequest.page = this.pagination.page;
    studentFeedbackRequest.limit = 10;

    this.studentFeedbackService.getStudentFeedbackList(studentFeedbackRequest)
      .subscribe(
        res => {
          // console.log(res);
          this.dataSource =  new MatTableDataSource(res.content);
          this.pagination.total = res.totalElements;
          this.pagination.currentPage = page;
          this.pagination.page = page;
          this.pagination.size = 10;
        },
        error => {
          this.pagination.page = olderPageValue;
          // console.log(error);
        });
  }

  applyFilter(filterValue: string) {
    // console.log("apply filter :",filterValue);
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  pageChanged(pageNo): void {
    pageNo = pageNo + 1;
    this.getList(pageNo);
  };

  displayedColumns = ['id', 'studentName', 'isLectureAttend', 'isLectureConducted', 'comment', 'rate', 'subject', 'facultyName', 'sem'];
}
