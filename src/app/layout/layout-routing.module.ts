import { Routes } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ChartsComponent } from './charts/charts.component';
import { UniversityComponent } from './university/university.component';
import { ProgramComponent } from './program/program.component';
import { InstituteComponent } from './institute/institute.component';
import { DepartmentComponent } from './department/department.component';

import { FacultyAttendanceComponent } from './faculty-attendance-list/faculty-attendance.component';
import { PrincipalAttendanceComponent } from './principal-attendance-list/principal-attendance.component';
import { HodAttendanceComponent } from './hod-attendance-list/hod-attendance.component';
import { CmAttendanceComponent } from './cm-attendance-list/cm-attendance.component';
import { DirectorAttendanceComponent } from './director-attendance-list/director-attendance.component';
import { StudentFeedbackComponent } from './student-feedback/student-feedback.component';
import { FacultyActivityComponent } from './faculty-activity/faculty-activity.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { TimeTableComponent } from './time-table/time-table.component';
import { LayoutComponent } from './layout.component';
import {DirectorAttendanceViewComponent} from './director-attendance-list/director-attendance-view/director-attendance-view.component';
import {DirectorTechnicalGraphComponent} from './director-attendance-list/director-technical-graph/director-technical-graph.component';
import {DirectorHigherGraphComponent} from './director-attendance-list/director-higher-graph/director-higher-graph.component';
import { FacultyListComponent } from './faculty-list/faculty-list.component';
export const LayoutRoutingModule: Routes = [
    {
        path: '',
        component: LayoutComponent,
        children: [
            {
                path: 'dashboard',
                component: DashboardComponent
            }
        ]
    },
    {
        path: '',
        children: [
            {
                path: 'charts',
                component: ChartsComponent
            }
        ]
    },
    {
        path: '',
        children: [
            {
                path: 'university',
                component: UniversityComponent
            }
        ]
    },
    {
        path: '',
        children: [
            {
                path: 'program',
                component: ProgramComponent
            }
        ]
    },
    {
        path: '',
        children: [
            {
                path: 'institute',
                component: InstituteComponent
            }
        ]
    },
    {
        path: '',
        children: [
            {
                path: 'department',
                component: DepartmentComponent
            }
        ]
    },
    {
        path: '',
        children: [
            {
                path: 'faculty-attendance',
                component: FacultyAttendanceComponent
            }
        ]
    },
    {
        path: '',
        children: [
            {
                path: 'principal-attendance',
                component: PrincipalAttendanceComponent
            }
        ]
    },
    {
        path: '',
        children: [
            {
                path: 'hod-attendance',
                component: HodAttendanceComponent
            }
        ]
    },
    {
        path: '',
        children: [
            {
                path: 'cm-attendance',
                component: CmAttendanceComponent
            }
        ]
    },
    {
        path: '',
        children: [
            {
                path: 'director-attendance',
                component: DirectorAttendanceComponent
            },
            {
                path: 'director-attendance/director-attendance-view',
                component: DirectorAttendanceViewComponent
            },
            {
                path: 'director-attendance/director-technical-graph',
                component: DirectorTechnicalGraphComponent
            },
            {
                path: 'director-attendance/director-higher-graph',
                component: DirectorHigherGraphComponent
            }
        ]
    },
    {
        path: '',
        children: [
            {
                path: 'student-feedback',
                component: StudentFeedbackComponent
            }
        ]
    },
    {
        path: '',
        children: [
            {
                path: 'faculty-activity',
                component: FacultyActivityComponent
            }
        ]
    },
    {
        path: '',
        children: [
            {
                path: 'staff-list',
                component: FacultyListComponent
            }
        ]
    },
    {
        path: '',
        children: [
            {
                path: 'change-password',
                component: ChangePasswordComponent
            }
        ]
    },
    {
        path: '',
        children: [
            {
                path: 'time-table',
                component: TimeTableComponent
            }
        ]
    }
];
