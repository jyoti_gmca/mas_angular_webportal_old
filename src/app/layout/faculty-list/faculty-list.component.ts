import * as _ from 'lodash';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { FacultyActivityService } from '../../Services/faculty-activity.service';
import { EditFacultyListComponent } from './edit-faculty-list/edit-faculty-list.component';
import { DeleteFacultyListComponent } from './delete-faculty-list/delete-faculty-list.component';
import { ShiftFacultyListComponent } from './shift-faculty-list/shift-faculty-list.component';
@Component({
  selector: 'app-faculty-list',
  templateUrl: './faculty-list.component.html',
  styleUrls: ['./faculty-list.component.scss']
})
export class FacultyListComponent implements OnInit {
    displayedColumns = [];
    rows = [];
    temp = [];
    department = [];
    selectDepartment;
    loading = false;
    singleRow = {};
    deleteRow = {};
    shiftRow = {};
    data: any;
  constructor(public dialog: MatDialog, private facultyService: FacultyActivityService) { }

  ngOnInit() {
      this.getFacultyList();
      this.getDepartment();
  }
  getFacultyList() {
      this.loading = true;
      const instituteId = parseInt(localStorage.getItem('instituteId'));
      this.facultyService.getFacultyListByInstituteId(instituteId).subscribe((res) => {
          if (res) {
              this.loading = false;
              this.rows = this.temp = res;
          }
      });
  }
  applyFilter(event) {
      if (event && event.target.value !== '') {
          const val = event.target.value.toLowerCase();
          const temp = this.temp.filter(function (d) {
              return d.name ? d.name.toLowerCase().indexOf(val) !== -1 || !val : !val;
          });
          this.rows = temp;
      } else {
          this.rows = this.temp;
      }
  }
    AddEditDialog(row?) {
      var dialogRef;
      if (row) {
          this.singleRow = row;
          dialogRef = this.dialog.open(EditFacultyListComponent, {
              width: '300px',
              height: '500px',
              disableClose: true,
              data: {id: row.id, name: row.name, email: row.email, phone: row.phone, role: row.role, department: row.departmentId, instituteId: row.instituteId, instituteTypeId: row.instituteTypeId}
          });
      } else {
          dialogRef = this.dialog.open(EditFacultyListComponent, {
              width: '300px',
              height: '500px',
              data: ''
          });
      }
        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                let tempArr = this.rows;
                const index = _.findIndex(this.rows, function(o) { return o.id === result.id });
                tempArr[index] = result;
                this.rows = [...tempArr];
            }
        });
    }
    deleteDialog(row) {
        this.deleteRow = row;
        const dialogRef = this.dialog.open(DeleteFacultyListComponent, {
            width: '400px',
            disableClose: true,
            data: {label: 'Are you Sure Want to delete this Record...', fid: row.id, instituteId: row.instituteId, instituteTypeId: row.instituteTypeId}
        });
        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                let tempArr = this.rows;
                const removeIndex = tempArr.map(function(item) { return item.id; }).indexOf(result.id);
                tempArr.splice(removeIndex, 1);
                this.rows = [...tempArr];
            }
        });
    }
    shiftDialog(row) {
        this.shiftRow = row;
        const dialogRef = this.dialog.open(ShiftFacultyListComponent, {
            width: '400px',
            disableClose: true,
            data: {label: 'Select Your Shift Timing:', instituteId: row.instituteId, fid: row.id}
        });
        dialogRef.afterClosed().subscribe(result => {
            console.log('The dialog was closed');
        });
    }
    getDepartment() {
        const instituteId = parseInt(localStorage.getItem('instituteId'));
        this.facultyService.getDeparmentByInstituteId(instituteId).subscribe((res) => {
            if (res) {
                this.department = res;
            }
        });
    }
    onDeartmentChange() {
      if (this.selectDepartment) {
          const result = this.temp.filter((t => t.departmentId === this.selectDepartment));
          this.rows = result;
      }
    }
}
