import {Component, Inject, OnInit} from '@angular/core';
import {FacultyActivityService} from '../../../Services/faculty-activity.service';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";

@Component({
  selector: 'app-shift-faculty-list',
  templateUrl: './shift-faculty-list.component.html',
  styleUrls: ['./shift-faculty-list.component.scss']
})
export class ShiftFacultyListComponent implements OnInit {
    shiftList = [];
    public selection: string;
  constructor(private facultyService: FacultyActivityService, public dialogRef: MatDialogRef<ShiftFacultyListComponent>,
              @Inject(MAT_DIALOG_DATA) public data) { }

  ngOnInit() {
      this.getShiftList();
  }
    getShiftList() {
      const instituteId = (this.data && this.data.instituteId) ? this.data.instituteId : parseInt(localStorage.getItem('instituteId'));
      this.facultyService.getShiftListByInstituteId(instituteId).subscribe((res) => {
            if (res) {
                this.shiftList = res;
                console.log(res);
            }
        });
    }
    onSubmitShift() {
        const sendData = {
            fId: this.data.fid,
            instituteId: this.data.instituteId,
            shiftId: parseInt(this.selection),
        };
        this.facultyService.updatedShiftData(sendData).subscribe((res) => {
            if (res) {
                this.dialogRef.close();
            }
        }, (error) => {
            this.dialogRef.close();
        });
    }
}
