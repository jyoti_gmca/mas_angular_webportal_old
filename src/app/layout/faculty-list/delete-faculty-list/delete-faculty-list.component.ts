import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
import {FacultyActivityService} from '../../../Services/faculty-activity.service';

@Component({
  selector: 'app-delete-faculty-list',
  templateUrl: './delete-faculty-list.component.html',
  styleUrls: ['./delete-faculty-list.component.scss']
})
export class DeleteFacultyListComponent implements OnInit {

  constructor(private facultyService: FacultyActivityService,
              public dialogRef: MatDialogRef<DeleteFacultyListComponent>,
              @Inject(MAT_DIALOG_DATA) public data) { }

  ngOnInit() {
  }
    onDeleteButton() {
        if (this.data && this.data.fid) {
            this.facultyService.deleteFacultyData(this.data.fid).subscribe((res) => {
                if (res) {
                    this.dialogRef.close(res);
                }
            }, (error) => {
                this.dialogRef.close();
            });
        }
    }
    onNoClick(): void {
        this.dialogRef.close();
    }
}
