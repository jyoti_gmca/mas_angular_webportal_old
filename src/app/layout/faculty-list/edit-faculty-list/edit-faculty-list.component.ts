import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {FacultyActivityService} from '../../../Services/faculty-activity.service';
import {DepartmentService} from '../../../Services/department.service';
import {FormControl, FormGroup} from '@angular/forms';
@Component({
    selector: 'edit-faculty-list',
    templateUrl: './edit-faculty-list.component.html',
    styleUrls: ['./edit-faculty-list.component.scss']
})
export class EditFacultyListComponent  implements OnInit {
    myControl = new FormControl();
    role = [
        {value: '2', viewValue: 'HOD'},
        {value: '1', viewValue: 'Faculty'},
        {value: '8', viewValue: 'Non-Teaching'}
    ];
    selected = {};
    department = {};
    departmentList: any = [];
    departmentName: any = [];
    filteredOptions: Observable<string[]>;
    popupName;
    constructor(
        private facultyService: FacultyActivityService,
        private departmentService: DepartmentService,
        public dialogRef: MatDialogRef<EditFacultyListComponent>,
        @Inject(MAT_DIALOG_DATA) public data) {}
ngOnInit() {
        this.popupName = (this.data) ? 'Edit' : 'Add';
        this.getDepartmentList();
        this.filteredOptions  = this.myControl.valueChanges
            .pipe(
                startWith(''),
                map(value => this._filter(value))
            );
        if (this.data) {
            const roleData = this.role.find(x => x.value === this.data.role.toString());
            this.selected = roleData && roleData.value;
        }
    }
    private _filter(value: string): string[] {
        const filterValue = value.toLowerCase();
        return this.departmentName.filter(option => option.toLowerCase().includes(filterValue));
    }
    getDepartmentList() {
        this.departmentService.getDepartmentList().subscribe((res) => {
            if (res) {
                this.departmentList = res;
                this.departmentName = res.map((data) => {
                    return data['name'];
                });
                if (this.data) {
                    const departmentData = this.departmentList.find(d => d.id === this.data.department);
                    this.department = departmentData.name;
                    this.myControl.setValue(this.department);
                }
            }
        });
    }
    editData(name) {
        var sendData;
        const departmentId = this.departmentList.find((d) => d.name === this.myControl.value);
        if (name === 'Add') {
            sendData = {
                name: this.data.name,
                email: this.data.email,
                phone: this.data.phone,
                role: this.selected,
                password: '12345',
                departmentId: departmentId.id,
                instituteId: this.data.instituteId,
                instituteTypeId: this.data.instituteTypeId,
                isDelete: false,
                activeFlag: false
            };
            this.facultyService.createFacultyData(sendData).subscribe((res) => {
                if (res) {
                    this.dialogRef.close();
                }
            }, (error) => {
                this.dialogRef.close();
            });
        } else {
            sendData = {
                id: this.data.id,
                name: this.data.name,
                email: this.data.email,
                phone: this.data.phone,
                role: this.selected,
                departmentId: departmentId.id,
                instituteId: this.data.instituteId,
                instituteTypeId: this.data.instituteTypeId,
            };
            this.facultyService.updatedFacultyData(sendData).subscribe((res) => {
                if (res) {
                    this.dialogRef.close(res);
                }
            }, (error) => {
                this.dialogRef.close();
            });
        }
    }
    onNoClick(): void {
        this.dialogRef.close();
    }
}
