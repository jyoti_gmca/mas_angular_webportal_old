import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Institute } from '../../../Model/institute-model';
import { InstituteService } from '../../../Services/institute.service';

@Component({
  selector: 'app-add-institute',
  templateUrl: './add-institute.component.html',
  styleUrls: ['./add-institute.component.scss']
})
export class AddInstituteComponent implements OnInit {

  isEdit = false;
  formGroup: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<AddInstituteComponent>,
    @Inject(MAT_DIALOG_DATA) public institute: Institute,
    private _formBuilder: FormBuilder,
    private _instituteService: InstituteService,
    // private _popupService: PopupService,
  ) {
    if (institute && institute.id) {
      this.isEdit = true;
    }
   }

  ngOnInit() {
    this.formGroup = this._formBuilder.group({
      name: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(32)]],
      description: ['', []],
      code: ['', [Validators.required]],
      link: ['', []],
      // active: [true, [Validators.required]]
    });
  }

  onClose(): void {
    this.dialogRef.close();
  }

  getNameErrorMessage() {
    let control = this.formGroup.controls.name;
    return control.hasError('required') ? '' :
      (control.hasError('minlength') || control.hasError('maxlength')) ? 'Name must be between 3 and 32' : '';
  }

  save(formValue, formValid) {
    // console.log("form value ::", formValue);
    // console.log("form valid ::", formValid);
    if (formValid) {
      let saveInstitute;
      let instituteObj: Institute = {
        name: this.institute.name,
        description: this.institute.description,
        code: this.institute.code,
        link: this.institute.link,
        id: this.institute.id
        // active: this.city.active,
        // stateId: Number(this.selectedStateId)
      }

      if (this.institute.id) {
        instituteObj.id = this.institute.id;
        saveInstitute = this._instituteService.updateInstitute(instituteObj);
      } else {
        saveInstitute = this._instituteService.createInstitute(instituteObj);
      }
        // console.log("institute request ::"+saveInstitute);
      saveInstitute.subscribe(
        res => {
          this.dialogRef.close();
          // console.log("Updated Successfully");
          // this.toastr.success(MESSAGE_SUCCESS.CITY_SAVED);
        },
        err => {
          // console.log("Error occured");
          // this._popupService.showError(err.message);
        });
    }
  }


}
