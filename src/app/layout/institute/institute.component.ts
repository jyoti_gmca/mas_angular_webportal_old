import { Component, OnInit, ViewChild } from '@angular/core';
import { InstituteService } from '../../Services/institute.service';
import { MatPaginator, MatSort, MatTableDataSource, MatDialog } from '@angular/material';
import { Institute } from '../../Model/institute-model';
import { AddInstituteComponent } from './add-institute/add-institute.component';
//import { Institute } from './institute-model';

@Component({
  selector: 'app-institute',
  templateUrl: './institute.component.html',
  styleUrls: ['./institute.component.scss']
})
export class InstituteComponent implements OnInit {
  universityList =[];
  dataSource: MatTableDataSource<Institute>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    public dialog: MatDialog,
    private instituteService: InstituteService
  ) {
  }

  ngOnInit() {
    this.getList();
    // this.dataSource.paginator = this.paginator;
    // this.dataSource.sort = this.sort;
  }

  getList(){

    this.instituteService.getInstituteList()
      .subscribe(
        res => {
          // console.log(res);
          // this.universityList = res;
          this.dataSource = res;
        //  this.customer = res as Customer;
        },
        error => console.log(error));
  }

  onDelete(id){
    this.instituteService.deleteInstitute(id)
      .subscribe(
        data => {
          // console.log("university data deleted successfully");
          this.getList();
        },
        error => console.log('ERROR: ' + error));
  }


  openDialog(program) {
    const dialogRef = this.dialog.open(AddInstituteComponent, {
      width: '600px',
      data: program
    });

    dialogRef.afterClosed().subscribe(result => {
     // this.getList(this.pagination.page);
     this.getList();
    });
  }
  displayedColumns = ['id', 'name', 'code', 'link', 'action'];
}
