import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ChangePasswordService } from '../../Services/change-password.service';
import { Router } from '@angular/router';

@Component({
    selector: 'app-changepassword',
    templateUrl: './change-password.component.html',
    styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {
    showMenu: string = '';
    userRole;
    isPrincipal =false;
    isHod =false;
    isFaculty =false;
    isDirector= false;

    searchObj = {
        email :"",
        oldPassword :"",
        newPassword :"",
        confirmPassword:""
      }

    formGroup: FormGroup;
    constructor(
        private _formBuilder: FormBuilder,
        private changePasswordService: ChangePasswordService,
        public router: Router
    ) {
        this.formGroup = this._formBuilder.group({
            email: ['', []],
            oldPassword: ['', []],
            newPassword: ['', []],
            confirmPassword: ['', []]
          });

    }

    ngOnInit() {
        this.userRole = localStorage.getItem('userRole');
        // console.log("userRole side bar ::"+this.userRole);

    }

    search(formValue, formValid) {
        if (formValid) {
        //   this.getList(0);
        let changePasswordRequest = {
            "email": "",
            "oldPassword": "",
            "newPassword": "",
            "retypePassword": ""
          }

          if (this.searchObj.email !== "") {
            changePasswordRequest.email = this.searchObj.email;
          } else {
            changePasswordRequest.email = "";
          }

          if (this.searchObj.oldPassword !== "") {
            changePasswordRequest.oldPassword = this.searchObj.oldPassword;
          } else {
            changePasswordRequest.oldPassword = "";
          }

          if (this.searchObj.newPassword !== "") {
            changePasswordRequest.newPassword = this.searchObj.newPassword;
          } else {
            changePasswordRequest.newPassword = "";
          }

          if (this.searchObj.confirmPassword !== "") {
            changePasswordRequest.retypePassword = this.searchObj.confirmPassword;
          } else {
            changePasswordRequest.retypePassword = "";
          }
          this.changePasswordService.changePassword(changePasswordRequest)
          .subscribe(
            res => {
            //   this.loading= false;
              // console.log("response size ::"+res);
              this.router.navigate(['/login']);
            },
            error => {
            //   this.loading= false;
              // console.log(error);
            });

        }
      }


}
