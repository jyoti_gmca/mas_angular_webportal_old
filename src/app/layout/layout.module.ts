import { CommonModule } from '@angular/common';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import {
    MatButtonModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatSidenavModule,
    MatToolbarModule,
    MatPaginatorModule,
    MatTableModule,
    MatFormFieldModule,
    MatCardModule,
    MatDatepickerModule,
    MatCheckboxModule,
    MatNativeDateModule,
    MatRadioModule
} from '@angular/material';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatDialogModule} from '@angular/material/dialog';
import {MatSelectModule} from '@angular/material/select';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import { NgxLoadingModule } from 'ngx-loading';
import {RouterModule} from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { TopnavComponent } from './components/topnav/topnav.component';
import { LayoutRoutingModule } from './layout-routing.module';
import { LayoutComponent } from './layout.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ChartsComponent } from './charts/charts.component';
import { UniversityComponent } from './university/university.component';
import { AddUniversityComponent } from './university/add-university/add-university.component';
import { ProgramComponent } from './program/program.component';
import { AddProgramComponent } from './program/add-program/add-program.component';
import { InstituteComponent } from './institute/institute.component';
import { DepartmentComponent } from './department/department.component';

import { FacultyAttendanceComponent } from './faculty-attendance-list/faculty-attendance.component';
import { PrincipalAttendanceComponent } from './principal-attendance-list/principal-attendance.component';
import { HodAttendanceComponent } from './hod-attendance-list/hod-attendance.component';
import { CmAttendanceComponent } from './cm-attendance-list/cm-attendance.component';
import { DirectorAttendanceComponent } from './director-attendance-list/director-attendance.component';
import { StudentFeedbackComponent } from './student-feedback/student-feedback.component';
import { FacultyActivityComponent } from './faculty-activity/faculty-activity.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { FacultyListComponent} from './faculty-list/faculty-list.component';
import { EditFacultyListComponent } from './faculty-list/edit-faculty-list/edit-faculty-list.component';
import { TimeTableComponent } from './time-table/time-table.component';
import {DirectorAttendanceViewComponent} from './director-attendance-list/director-attendance-view/director-attendance-view.component';
import {DirectorTechnicalGraphComponent} from './director-attendance-list/director-technical-graph/director-technical-graph.component';
import {DirectorHigherGraphComponent} from './director-attendance-list/director-higher-graph/director-higher-graph.component';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { StatModule } from '../shared/modules/stat/stat.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import { DeleteFacultyListComponent } from './faculty-list/delete-faculty-list/delete-faculty-list.component';
import { ShiftFacultyListComponent } from './faculty-list/shift-faculty-list/shift-faculty-list.component';
import { AddInstituteComponent } from './institute/add-institute/add-institute.component';
import { AddDepartmentComponent } from './department/add-department/add-department.component';
import { CmAttendanceViewComponent } from './cm-attendance-list/cm-attendance-view/cm-attendance-view.component';
import { CmHigherGraphComponent } from './cm-attendance-list/cm-higher-graph/cm-higher-graph.component';
import {CmTechnicalGraphComponent} from './cm-attendance-list/cm-technical-graph/cm-technical-graph.component';
@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(LayoutRoutingModule),
        MatToolbarModule,
        MatButtonModule,
        MatSidenavModule,
        MatIconModule,
        MatInputModule,
        MatMenuModule,
        MatListModule,
        TranslateModule,
        FormsModule,
        ReactiveFormsModule,
        MatPaginatorModule,
        MatTableModule,
        MatFormFieldModule,
        MatDialogModule,
        MatCardModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatSelectModule,
        MatTooltipModule,
        MatSlideToggleModule,
        MatCheckboxModule,
        MatRadioModule,
        NgxLoadingModule.forRoot({}),
        ChartsModule,
        StatModule,
        FlexLayoutModule,
        MatAutocompleteModule,
        NgxDatatableModule
    ],
    declarations: [
        LayoutComponent,
        TopnavComponent,
        SidebarComponent,
        DashboardComponent,
        ChartsComponent,
        UniversityComponent,
        AddUniversityComponent,
        ProgramComponent,
        AddProgramComponent,
        InstituteComponent,
        DepartmentComponent,
        FacultyAttendanceComponent,
        PrincipalAttendanceComponent,
        HodAttendanceComponent,
        CmAttendanceComponent,
        DirectorAttendanceComponent,
        StudentFeedbackComponent,
        FacultyActivityComponent,
        ChangePasswordComponent,
        TimeTableComponent,
        DirectorAttendanceViewComponent,
        DirectorTechnicalGraphComponent,
        DirectorHigherGraphComponent,
        FacultyListComponent,
        EditFacultyListComponent,
        DeleteFacultyListComponent,
        ShiftFacultyListComponent,
        AddInstituteComponent,
        AddDepartmentComponent,
        CmAttendanceViewComponent,
        CmHigherGraphComponent,
        CmTechnicalGraphComponent
    ],
    entryComponents: [AddUniversityComponent, AddProgramComponent, AddInstituteComponent, AddDepartmentComponent, EditFacultyListComponent, DeleteFacultyListComponent, ShiftFacultyListComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
    providers: [MatDatepickerModule],
    exports: [ChartsComponent, DashboardComponent, TopnavComponent, SidebarComponent]
})
export class LayoutModule {}
