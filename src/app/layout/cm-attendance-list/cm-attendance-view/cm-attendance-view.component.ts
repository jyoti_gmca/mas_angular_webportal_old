import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA, MatTableDataSource } from '@angular/material';
// import { Department } from '../department-model';
 import { CmAttendanceService } from '../../../Services/cm-attendance.service';
import { CmAttendance } from '../../../Model/cm-attendance-model';

@Component({
  selector: 'app-cm-attendance-view',
  templateUrl: './cm-attendance-view.component.html',
  styleUrls: ['./cm-attendance-view.component.scss']
})
export class CmAttendanceViewComponent implements OnInit {
  isEdit = false;
  formGroup: FormGroup;
  dataSource: MatTableDataSource<CmAttendance>;

  userId;
  instituteId;
  departmentId;
  userRole;

  public loading= false;

  searchObj = {
    searchText: "",
    instituteId: "",
    departmentId: "",
    startDate: "",
    endDate: "",
    facultyId: ""
  }

  pagination ={
    "page":0,
    "size":10,
    "total":0,
    "currentPage":0
  }

  instituteList = [];
  departmentList = [];
  facultyList = [];

  constructor(
   // public dialogRef: MatDialogRef<DirectorAttendanceViewComponent>,
    // @Inject(MAT_DIALOG_DATA) public department: Department,
    private _formBuilder: FormBuilder,
    private facultyattendanceService: CmAttendanceService,
    // private _popupService: PopupService,
  ) {
  }

  ngOnInit() {
    this.formGroup = this._formBuilder.group({
      searchText: ['', []],
      instituteId: ['', []],
      departmentId: ['', []],
      startDate: ['', []],
      endDate: ['', []],
      facultyId: ['', []]
    });

    this.userId= localStorage.getItem('userId');
    this.instituteId = localStorage.getItem('instituteId');
    this.departmentId = localStorage.getItem('departmentId');
    this.userRole = localStorage.getItem('userRole');
    // console.log("user role in cm tabular::",this.userRole);
    if(this.userRole == 6){
      this.getInstituteDropdown(1);
    }
    if(this.userRole == 7){
      this.getInstituteDropdown(2);
    }

   // this.getDepartmentDropdown();
    // this.getFacultyDropdown();
    this.getList(1);

  }

  search(formValue, formValid) {
    if (formValid) {
      this.getList(1);
    }
  }

  getInstituteName(instituteId) {
    let institute = this.instituteList.find(obj => { return obj.id = instituteId });
    if (institute)
      return institute.name;
  }

  getInstituteDropdown(instituteTypeId) {

    this.facultyattendanceService.getInstituteList(instituteTypeId)
      .subscribe(res => {
        this.instituteList = res;
      }, err => {
        //this._popupService.showError(err.message);
      });
  }

  onInstituteChange() {
    // this.searchObj.departmentId = '0';
    // this.departmentList = [];
    // if (this.searchObj.instituteId == '0') {
    //   return;
    // }
      this.getDepartmentDropdown(this.searchObj.instituteId);
  }

  getDepartmentDropdown(instituteId) {
    this.departmentList = [];
    this.facultyList=[];
    if (!instituteId) {
      return;
    }
    this.facultyattendanceService.getDepartmentByInstitute(instituteId)
      .subscribe(res => {
        this.departmentList = res;
      }, err => {
        // this._popupService.sh   owError(err.message);
      });
  }

  onDeartmentChange(){
    // console.log("on deartment change ::",this.searchObj.departmentId);
    this.getFacultyDropdown(this.searchObj.departmentId);
  }

  getFacultyDropdown(departmentId){
    this.facultyList = [];
    if (!departmentId) {
      return;
    }
    this.facultyattendanceService.getFacultyByDepartmentId(departmentId)
      .subscribe(res => {
        this.facultyList = res;
      }, err => {
        // this._popupService.sh   owError(err.message);
      });
  }

  // getDepartmentDropdown() {
  //   this.facultyattendanceService.getDepartmentList()
  //     .subscribe(res => {
  //       this.departmentList = res;
  //     }, err => {
  //       //this._popupService.showError(err.message);
  //     });
  // }

  // getFacultyDropdown() {
  //   this.facultyattendanceService.getFacultyList()
  //     .subscribe(res => {
  //       this.facultyList = res;
  //     }, err => {
  //     });
  // }

  pageChanged(pageNo): void {
    // console.log("page changed ::",pageNo);
    pageNo = pageNo+1;
    this.getList(pageNo);
  };

  getList(page: number) {

    this.loading = true;
    var olderPageValue = this.pagination.page;
    this.pagination.page = page;
    this.pagination.page = page -1;

    let facultyAttendanceRequest = {
      "departmentId": "",
      "instituteId": "",
      "facultyId": "",
      "startDate": "",
      "endDate": "",
      "searchText": "",
      "page": 0,
      "limit": 10,
      "instituteTypeId": "1"
    }

    if (this.searchObj.departmentId !== "") {
      facultyAttendanceRequest.departmentId = this.searchObj.departmentId;
    } else {
      facultyAttendanceRequest.departmentId = "";
    }

    if (this.searchObj.facultyId !== "") {
      facultyAttendanceRequest.facultyId = this.searchObj.facultyId;
    } else {
      facultyAttendanceRequest.facultyId = "";
    }

    if (this.searchObj.instituteId !== "") {
      facultyAttendanceRequest.instituteId = this.searchObj.instituteId;
    } else {
      facultyAttendanceRequest.instituteId = "";
    }

    if (this.searchObj.startDate != "") {
      facultyAttendanceRequest.startDate = this.searchObj.startDate;
    } else {
      facultyAttendanceRequest.startDate = "";
    }

    if (this.searchObj.endDate !== "") {
      facultyAttendanceRequest.endDate = this.searchObj.endDate;
    } else {
      facultyAttendanceRequest.endDate = "";
    }

     if(this.userRole == 6){
       facultyAttendanceRequest.instituteTypeId="1";
     }
     if (this.userRole == 7){
       facultyAttendanceRequest.instituteTypeId= "2";
     }

    facultyAttendanceRequest.page = this.pagination.page;
    facultyAttendanceRequest.limit = 10;

    // console.log("request send ::", facultyAttendanceRequest);
    this.facultyattendanceService.getFacultyAttendanceList(facultyAttendanceRequest)
      .subscribe(
        res => {
          this.loading = false;
          this.dataSource = res.content;
          this.pagination.total = res.totalElements;
          this.pagination.currentPage = page;
          this.pagination.page = page;
        },
        error => {
          this.loading = false;
          this.pagination.page = olderPageValue;
          // console.log(error)
        });
  }

  displayedColumns = ['position', 'facultyId', 'attendanceDate', 'instituteName', 'departmentName', 'inTime', 'outTime', 'attendance'];

}
