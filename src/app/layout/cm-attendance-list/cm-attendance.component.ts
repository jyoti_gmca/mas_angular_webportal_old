import { CmAttendance } from '../../Model/cm-attendance-model';
import { Component, OnInit, ViewChild } from '@angular/core';
import { CmAttendanceService } from '../../Services/cm-attendance.service';
import { MatPaginator, MatSort, MatTableDataSource, MatDialog } from '@angular/material';
import { FormBuilder, FormGroup } from '@angular/forms';
import { DashboardService } from '../../Services/dashboard.service';
import { element } from 'protractor';
import { reduce } from 'rxjs/operators';
import { Router } from '@angular/router';
//import { Institute } from './institute-model';

@Component({
  selector: 'app-cm-attendance',
  templateUrl: './cm-attendance.component.html',
  styleUrls: ['./cm-attendance.component.scss']
})
export class CmAttendanceComponent implements OnInit {
  universityList = [];
  dataSource: MatTableDataSource<CmAttendance>;
  // @ViewChild(MatPaginator) paginator: MatPaginator;
  // @ViewChild(MatSort) sort: MatSort;

  totalTechRegFaculty;
  totalHigherRegFaculty;

  presentTechFaculty=0;
  presentHigherFaculty=0;

  absentTechFaculty =0;
  absentHigherFaculty=0;

  isDirectorTechnical = false;
  isDirectorHigher = false;

  searchObj = {
    searchText: "",
    instituteId: "",
    departmentId: "",
    startDate: "",
    endDate: "",
    facultyId: ""
  }

  instituteList = [];
  departmentList = [];
  facultyList = [];

  userId;
  instituteId;
  departmentId;
  userRole;

  pagination ={
    "page":0,
    "size":5,
    "total":0,
    "currentPage":0
  }

  formGroup: FormGroup;
  chartForm:FormGroup;

  // For charts
  places: Array<any> = [];
  isPrincipalChartReady = false;
  testArray;

  barChartOptions = {
    scaleOverride: true,
    scaleStartValue: 0,
    isStacked: true,
    responsive: true,
    // responsiveAnimationDuration: 0,
    //  maintainAspectRatio: true,
    scales: {
      yAxes: [{
        stacked: true,
        display: true,
        ticks: {
          beginAtZero: true,
          steps: 1,
          stepValue: 4,
          //max: 20
        }
      }],
      xAxes: [{
        stacked: true,
        display: true,
      }]
    },
    title: {
      display: true,
//      text: 'Weekly Attendance',
      fontSize: 20,
      fontColor: "#2f4f4f",
    }
  };

  public barChartLabels: string[] = [];
  public barChartType: string = 'bar';
  public barChartLegend: boolean = true;
  //public barChartColor = ["#FF0000", "#00FF00"];
  barChartData: Array<any> = [];
  public barchartColor: Array<any> = [
    {
      // green
      backgroundColor: 'rgba(119, 224, 108, 1)',
      borderColor: 'rgba(119, 224, 108, 1)',
      pointBackgroundColor: 'rgba(119, 224, 108, 1)',
      pointBorderColor: '#126312',
      pointHoverBackgroundColor: '#126312',
      pointHoverBorderColor: 'rgba(119, 224, 108, 1)'
    },
    {
      // dark red
      backgroundColor: 'rgba(245, 92, 92, 1)',
      borderColor: 'rgba(245, 92, 92, 1)',
      pointBackgroundColor: 'rgba(245, 92, 92, 1)',
      pointBorderColor: '#f55c5c',
      pointHoverBackgroundColor: '#f55c5c',
      pointHoverBorderColor: 'rgba(245, 92, 92, 1)'
    },
  ];

  testList: any[];


  constructor(
    private router: Router,
    public dialog: MatDialog,
    private facultyattendanceService: CmAttendanceService,
    private _formBuilder: FormBuilder,
    private dashboardService: DashboardService,
  ) {
    this.formGroup = this._formBuilder.group({
      searchText: ['', []],
      instituteId: ['', []],
      departmentId: ['', []],
      startDate: ['', []],
      endDate: ['', []],
      facultyId: ['', []]
    });

    this.chartForm = this._formBuilder.group({
      isMonthly: [true, []],
      isWeekly: [false, []]
    });
  }

  ngOnInit() {
    this.userId = localStorage.getItem('userId');
    this.userRole = localStorage.getItem('userRole');
    this.instituteId = localStorage.getItem('instituteId');
    this.departmentId = localStorage.getItem('departmentId');

    if(this.userRole ==6){
      this.isDirectorTechnical = true;
    }
    if(this.userRole == 7){
      this.isDirectorHigher = true;
    }

    this.principalChart("isMonthly");
    // this.getList(0);
    this.registeredFaculty();
    this.presentFaculty();
    this.absentFaculty();
  }

  registeredFaculty() {
    this.facultyattendanceService.getTotalRegisteredFaculty()
      .subscribe(
        res => {
          res.forEach(element => {
            if(element.instituteType == "Technical Education"){
              this.totalTechRegFaculty= element.count;
            }
            if(element.instituteType == "Higher Education"){
              this.totalHigherRegFaculty= element.count;
            }
          });
        },
        error => {
          console.log(error)
        });
  }

  presentFaculty() {
    this.facultyattendanceService.getTotalPresentFaculty()
      .subscribe(
        res => {
          res.forEach(element => {
            if(element.instituteType == "Technical Education"){
              this.presentTechFaculty= element.count;
            }
            if(element.instituteType == "Higher Education"){
              this.presentHigherFaculty= element.count;
            }
          });
        },
        error => {
          console.log(error)
        });
  }

  absentFaculty() {
    this.facultyattendanceService.getTotalAbsentFaculty()
      .subscribe(
        res => {
          res.forEach(element => {
            if(element.instituteType == "Technical Education"){
              this.absentTechFaculty= element.count;
            }
            if(element.instituteType == "Higher Education"){
              this.absentHigherFaculty= element.count;
            }
          });
        },
        error => {
          console.log(error)
        });
  }

  // Methods for charts

  public chartClicked(e: any): void {
    e.active.forEach(element => {
      if(element._model.label == "Technical Education"){
        this.router.navigate(['/director-attendance/director-technical-graph']);
      }
      if(element._model.label == "Higher Education"){
        this.router.navigate(['/director-attendance/director-higher-graph']);
      }
    });
  }

  public chartHovered(e: any): void {
    console.log(e);
  }

  public randomize(): void {
    // Only Change 3 values
    const data = [
      Math.round(Math.random() * 100),
      59,
      80,
      Math.random() * 100,
      56,
      Math.random() * 100,
      40
    ];
    const clone = JSON.parse(JSON.stringify(this.barChartData));
    clone[0].data = data;
    this.barChartData = clone;

  }

  principalChart(value) {
    let dashboardRequest = {
    }

    this.barChartLabels = [];
    this.dashboardService.getDirectorDashboardChart(dashboardRequest)
      .subscribe(
        res => {
          console.log("director attendance list ::", res);
          this.testArray = res;
          var presentData = [];
          var absentData = [];

          let newChartData: Array<{ data: Array<any>, label: string, color: string }> = [];
          //  this.barChartLabels =['02-nov-2018', '01-nov-2018', '31-oct-2018'];
          var instituteListData = [];
          this.testArray.forEach(element => {

            if(this.isDirectorTechnical && element.instituteName == "Technical Education"){
            if (element.instituteName) {
              instituteListData.push(element.instituteName);
            }
            // if (element.status) {
              presentData.push(element.count);
            // }
            // if (!element.status) {
              // newChartData.push({ data: element.count, label: 'Absent'})
              absentData.push(element.absentCount);
            // }
          }
          if(this.isDirectorHigher && element.instituteName == "Higher Education"){
            if (element.instituteName) {
              instituteListData.push(element.instituteName);
            }
              presentData.push(element.count);
              absentData.push(element.absentCount);
          }
          });

          this.barChartLabels = Array.from(new Set(instituteListData));
          //console.log("Present data :", presentData, "Absent data  ::", absentData);
          newChartData.push({ data: presentData, label: 'Present', color: '#00FF44' }, { data: absentData, label: 'Absent', color: "#FF0002" });

          // newChartData.push({ data: [1,1,2,3,2,3,4], label: 'Present' }, { data: [1,1,4,2,3,4,0], label: 'Absent' });
          this.barChartData = newChartData;
          // console.log("bar chart data ::" + this.barChartData);
          // console.log("bar chart labes ::" + this.barChartLabels);
          this.isPrincipalChartReady = true;

        },
        error => console.log(error));
  }
  displayedColumns = ['position', 'facultyId', 'attendanceDate', 'instituteName', 'departmentName', 'inTime', 'outTime', 'attendance'];
}
