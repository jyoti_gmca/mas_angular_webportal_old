import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA, MatTableDataSource } from '@angular/material';
 import { CmAttendanceService } from '../../../Services/cm-attendance.service';
import { CmAttendance } from '../../../Model/cm-attendance-model';
import { DashboardService } from '../../../Services/dashboard.service';

@Component({
  selector: 'app-cm-higher-graph',
  templateUrl: './cm-higher-graph.component.html',
  styleUrls: ['./cm-higher-graph.component.scss']
})
export class CmHigherGraphComponent implements OnInit {
  isEdit = false;
  formGroup: FormGroup;
  dataSource: MatTableDataSource<CmAttendance>;

  userId;
  instituteId;
  departmentId;

  isPrincipalChartReady = false;
  testArray;

  barChartOptions = {
    scaleOverride: true,
    scaleStartValue: 0,
    isStacked: true,
    responsive: true,
    // responsiveAnimationDuration: 0,
    //  maintainAspectRatio: true,
    scales: {
      yAxes: [{
        stacked: true,
        display: true,
        ticks: {
          beginAtZero: true,
          steps: 1,
          stepValue: 4,
          //max: 20
        }
      }],
      xAxes: [{
        stacked: true,
        display: true,
        labelAngle: -50,
				labelFontColor: "rgb(0,75,141)"
      }]
    },
    title: {
      display: true,
      // text: 'Weekly Attendance',
      fontSize: 20,
      fontColor: "#2f4f4f",
    }
  };

  public barChartLabels: string[] = [];
  public barChartType: string = 'bar';
  public barChartLegend: boolean = true;
  //public barChartColor = ["#FF0000", "#00FF00"];
  public barchartColor: Array<any> = [
    {
      // red
      backgroundColor: 'rgba(119, 224, 108, 1)',
      borderColor: 'rgba(119, 224, 108, 1)',
      pointBackgroundColor: 'rgba(119, 224, 108, 1)',
      pointBorderColor: '#126312',
      pointHoverBackgroundColor: '#126312',
      pointHoverBorderColor: 'rgba(119, 224, 108, 1)'
    },
    {
      // dark green
      backgroundColor: 'rgba(245, 92, 92, 1)',
      borderColor: 'rgba(245, 92, 92, 1)',
      pointBackgroundColor: 'rgba(245, 92, 92, 1)',
      pointBorderColor: '#f55c5c',
      pointHoverBackgroundColor: '#f55c5c',
      pointHoverBorderColor: 'rgba(245, 92, 92, 1)'
    },
  ];

  barChartData: Array<any> = [];
  testList: any[];


  constructor(
    private _formBuilder: FormBuilder,
    private facultyattendanceService: CmAttendanceService,
    private dashboardService: DashboardService,
  ) {
  }

  ngOnInit() {

    this.userId= localStorage.getItem('userId');
    this.instituteId = localStorage.getItem('instituteId');
    this.departmentId = localStorage.getItem('departmentId');

    this.principalChart("isMonthly");
  }

  principalChart(value) {
    let dashboardRequest = {
     "instituteTypeId":2
    }

    this.barChartLabels = [];
    this.dashboardService.getDirectorChartInstituteWise(dashboardRequest)
      .subscribe(
        res => {
          // console.log("director faculty attendance list ::",res);
          this.testArray = res;
          var presentData = [];
          var absentData = [];

          let newChartData: Array<{ data: Array<any>, label: string, color: string }> = [];
          //  this.barChartLabels =['02-nov-2018', '01-nov-2018', '31-oct-2018'];
          var instituteListData = [];
          this.testArray.forEach(element => {

            if (element.instituteName) {
              instituteListData.push(element.instituteName);
            }
              presentData.push(element.count);
              // newChartData.push({ data: element.count, label: 'Absent'})
              absentData.push(element.absentCount);
          });

         // this.barChartLabels =  Array.from(new Set(instituteListData ));
          // console.log("Present data :", presentData, "Absent data  ::", absentData);
          newChartData.push({ data: presentData, label: 'Present', color: '#00FF44' }, { data: absentData, label: 'Absent', color: "#FF0002" });

          // newChartData.push({ data: [1,1,2,3,2,3,4], label: 'Present' }, { data: [1,1,4,2,3,4,0], label: 'Absent' });
          this.barChartData = newChartData;
          this.barChartLabels = instituteListData;
          // // console.log("bar chart data ::"+this.barChartData);
          // console.log("bar chart labes ::"+this.barChartLabels);
          this.isPrincipalChartReady = true;

        },
        error => console.log(error));
  }

}
