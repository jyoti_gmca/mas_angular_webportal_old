import { Component, OnInit, ViewChild } from '@angular/core';
import { FacultyAttendanceService} from '../../Services/faculty-attendance.service';
import { MatPaginator, MatSort, MatTableDataSource, MatDialog } from '@angular/material';
import { FacultyAttendance } from '../../Model/faculty-attendance-model';
import { FormBuilder, FormGroup } from '@angular/forms';
//import { Institute } from './institute-model';

@Component({
  selector: 'app-faculty-attendance',
  templateUrl: './faculty-attendance.component.html',
  styleUrls: ['./faculty-attendance.component.scss']
})
export class FacultyAttendanceComponent implements OnInit {
  universityList =[];
  dataSource: MatTableDataSource<FacultyAttendance>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  public loading = false;
  searchObj = {
    searchText: "",
    instituteId: "",
    departmentId: "",
    startDate:"",
    endDate:""
  }

  pagination = {
    "page": 0,
    "size": 10,
    "total": 0,
    "currentPage": 0
  }

  instituteList = [];
  departmentList = [];

  formGroup: FormGroup;
  userId;

  constructor(
    public dialog: MatDialog,
    private facultyattendanceService: FacultyAttendanceService,
    private _formBuilder: FormBuilder,
  ) {
    this.formGroup = this._formBuilder.group({
      searchText: ['', []],
      instituteId: ['', []],
      departmentId: ['', []],
      startDate: ['',[]],
      endDate: ['',[]]
    });
  }

  ngOnInit() {
    this.userId = localStorage.getItem('userId');
    // console.log("user id ::"+this.userId);
    // this.getInstituteDropdown();
    // this.getDepartmentDropdown();
    this.getList(1);
    // this.dataSource.paginator = this.paginator;
    // this.dataSource.sort = this.sort;
  }

  search(formValue, formValid) {
    if (formValid) {
      this.getList(1);
    }
  }

  pageChanged(pageNo): void {
    pageNo = pageNo + 1;
    this.getList(pageNo);
  };

  getList(page: number){
    this.loading = true;
    // console.log("page number ::", page);
    var olderPageValue = this.pagination.page;
    this.pagination.page = page - 1;

    let facultyAttendanceRequest= {
      "departmentId" :"",
      "instituteId" :"",
      "startDate" : "",
      "endDate" :"",
      "searchText":"",
      "facultyId":"",
      "page": 0,
      "limit": 10
    }

    if (this.searchObj.startDate != "") {
      facultyAttendanceRequest.startDate = this.searchObj.startDate;
    } else {
      facultyAttendanceRequest.startDate ="";
    }

    if (this.searchObj.endDate !== "") {
      facultyAttendanceRequest.endDate = this.searchObj.endDate;
    } else {
      facultyAttendanceRequest.endDate ="";
    }

    facultyAttendanceRequest.facultyId = this.userId;
    facultyAttendanceRequest.page = this.pagination.page;
    facultyAttendanceRequest.limit = 10;
    // console.log("request send ::", facultyAttendanceRequest);
    this.facultyattendanceService.getFacultyAttendanceList(facultyAttendanceRequest)
      .subscribe(
        res => {
          this.loading= false;
          // console.log("Faculty Attendance List ::",res);
          this.dataSource = res.content;

          this.pagination.total = res.totalElements;
          this.pagination.currentPage = page;
          this.pagination.page = page;
          this.pagination.size = 10;
        },
        error => {
          this.loading= false;
          console.log(error);
        });

  }

  onDelete(id){
    this.facultyattendanceService.deleteFacultyAttendance(id)
      .subscribe(
        data => {
          // console.log("faculty attendance data deleted successfully");
          this.getList(1);
        },
        error => console.log('ERROR: ' + error));
  }


  // openDialog(program) {
  //   const dialogRef = this.dialog.open(AddInstituteComponent, {
  //     width: '600px',
  //     data: program
  //   });

  //   dialogRef.afterClosed().subscribe(result => {
  //    // this.getList(this.pagination.page);
  //    this.getList();
  //   });
  // }
  displayedColumns = ['position', 'attendanceDate', 'instituteName', 'departmentName','inTime', 'outTime', 'attendance' ];
}
