import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Department } from '../../../Model/department-model';
import { DepartmentService } from '../../../Services/department.service';

@Component({
  selector: 'app-add-department',
  templateUrl: './add-department.component.html',
  styleUrls: ['./add-department.component.scss']
})
export class AddDepartmentComponent implements OnInit {
  isEdit = false;
  formGroup: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<AddDepartmentComponent>,
    @Inject(MAT_DIALOG_DATA) public department: Department,
    private _formBuilder: FormBuilder,
    private _departmentService: DepartmentService,
    // private _popupService: PopupService,
  ) {
    if (department && department.id) {
      this.isEdit = true;
    }
   }

  ngOnInit() {
    this.formGroup = this._formBuilder.group({
      name: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(32)]],
      description: ['', []],
      code: ['', [Validators.required]],
      link: ['', []],
      // active: [true, [Validators.required]]
    });
  }

  onClose(): void {
    this.dialogRef.close();
  }

  getNameErrorMessage() {
    let control = this.formGroup.controls.name;
    return control.hasError('required') ? '' :
      (control.hasError('minlength') || control.hasError('maxlength')) ? 'Name must be between 3 and 32' : '';
  }

  save(formValue, formValid) {
    console.log("form value ::", formValue);
    console.log("form valid ::", formValid);
    if (formValid) {
      let saveDepartment;
      let departmentObj: Department = {
        name: this.department.name,
        description: this.department.description,
        code: this.department.code,
        link: this.department.link,
        id: this.department.id
        // active: this.city.active,
        // stateId: Number(this.selectedStateId)
      }

      if (this.department.id) {
        departmentObj.id = this.department.id;
        saveDepartment = this._departmentService.updateDepartment(departmentObj);
      } else {
        saveDepartment = this._departmentService.createDepartment(departmentObj);
      }
        console.log("department request ::"+saveDepartment);
      saveDepartment.subscribe(
        res => {
          this.dialogRef.close();
          console.log("Updated Successfully");
          // this.toastr.success(MESSAGE_SUCCESS.CITY_SAVED);
        },
        err => {
          console.log("Error occured");
          // this._popupService.showError(err.message);
        });
    }
  }
}
