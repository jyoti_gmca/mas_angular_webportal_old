import { Component, OnInit, ViewChild } from '@angular/core';
import { DepartmentService } from '../../Services/department.service';
import { MatPaginator, MatSort, MatTableDataSource, MatDialog } from '@angular/material';
import { Department } from '../../Model/department-model';
import { AddDepartmentComponent } from './add-department/add-department.component';

@Component({
  selector: 'app-department',
  templateUrl: './department.component.html',
  styleUrls: ['./department.component.scss']
})
export class DepartmentComponent implements OnInit {
  departmentList =[];
  dataSource: MatTableDataSource<Department>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    public dialog: MatDialog,
    private departmentService: DepartmentService
  ) {
  }

  ngOnInit() {
    this.getList();
    // this.dataSource.paginator = this.paginator;
    // this.dataSource.sort = this.sort;
  }

  getList(){

    this.departmentService.getDepartmentList()
      .subscribe(
        res => {
          console.log(res);
          // this.departmentList = res;
          this.dataSource = res;
        //  this.customer = res as Customer;
        },
        error => console.log(error));
  }

  onDelete(id){
    this.departmentService.deleteDepartment(id)
      .subscribe(
        data => {
          console.log("department data deleted successfully");
          this.getList();
        },
        error => console.log('ERROR: ' + error));
  }

  openDialog(department) {
    const dialogRef = this.dialog.open(AddDepartmentComponent, {
      width: '600px',
      data: department
    });

    dialogRef.afterClosed().subscribe(result => {
     // this.getList(this.pagination.page);
     this.getList();
    });
  }

  displayedColumns = ['id', 'name', 'code', 'link', 'action'];
}
