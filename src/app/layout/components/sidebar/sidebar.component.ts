import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
    showMenu: string = '';
    userRole;
    isPrincipal =false;
    isHod =false;
    isFaculty =false;
    isDirectorHigher= false;
    isDirectorTechnical= false;
    isPS = false;
    constructor(
    ) {
    }

    ngOnInit() {
        this.userRole = localStorage.getItem('userRole');
        console.log("userRole side bar ::"+this.userRole);
        if(this.userRole ==1){
            this.isFaculty = true;
        }
        else if(this.userRole ==2){
            this.isHod= true;
        }
        else if(this.userRole ==3){
            console.log("is principal true !!");
            this.isPrincipal=true;
        }
        else if(this.userRole ==5){
            console.log("is ps true !!");
            this.isPS=true;
        }
        else if(this.userRole ==6){
            console.log("is director true !!");
            this.isDirectorTechnical=true;
        }
        else if(this.userRole ==7){
            console.log("is director true !!");
            this.isDirectorHigher=true;
        }
    }

    addExpandClass(element: any) {
        if (element === this.showMenu) {
            this.showMenu = '0';
        } else {
            this.showMenu = element;
        }
    }
}
