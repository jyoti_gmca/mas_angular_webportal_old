import { Component, OnInit, ViewChild } from '@angular/core';
import { PrincipalAttendanceService } from '../../Services/principal-attendance.service';
import { MatPaginator, MatSort, MatTableDataSource, MatDialog } from '@angular/material';
import { PrincipalAttendance } from '../../Model/principal-attendance-model';
import { FormBuilder, FormGroup } from '@angular/forms';
import { DashboardService } from '../../Services/dashboard.service';
import * as moment from 'moment';
import { $ } from 'protractor';
//import { Institute } from './institute-model';

@Component({
  selector: 'app-principal-attendance',
  templateUrl: './principal-attendance.component.html',
  styleUrls: ['./principal-attendance.component.scss']
})
export class PrincipalAttendanceComponent implements OnInit {
  universityList = [];
  dataSource: MatTableDataSource<PrincipalAttendance>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  public loading = false;

  searchObj = {
    searchText: "",
    instituteId: "",
    departmentId: "",
    startDate: "",
    endDate: "",
    facultyId: ""
  }

  instituteList = [];
  departmentList = [];
  facultyList = [];
  userId;
  instituteId;
  departmentId;
  displayInstituteName;
  pagination = {
    "page": 0,
    "size": 10,
    "total": 0,
    "currentPage": 0
  }

  // For charts
  places: Array<any> = [];
  isPrincipalChartReady = false;
  testArray;
  totalStudentAttendanceList;

  barChartOptions = {
    scaleOverride: true,
    scaleStartValue: 0,
    isStacked: true,
    scales: {
      yAxes: [{
        stacked: true,
        display: true,
        ticks: {
          beginAtZero: true,
          steps: 1,
          stepValue: 4,
        }
      }],
      xAxes: [{
        stacked: true,
        display: true,

      }]
    },
    title: {
      display: true,
      // text: 'Weekly Attendance'
    }
  };
  public barChartLabels: string[] = [];
  public barChartType: string = 'bar';
  public barChartLegend: boolean = true;
  // public barchartColor : string[] =["#ff1212", "#12ff12"];
  public barchartColor: Array<any> = [
    {
      // green
      backgroundColor: 'rgba(119, 224, 108, 1)',
      borderColor: 'rgba(119, 224, 108, 1)',
      pointBackgroundColor: 'rgba(119, 224, 108, 1)',
      pointBorderColor: '#126312',
      pointHoverBackgroundColor: '#126312',
      pointHoverBorderColor: 'rgba(119, 224, 108, 1)'
    },
    {
      // dark red
      backgroundColor: 'rgba(245, 92, 92, 1)',
      borderColor: 'rgba(245, 92, 92, 1)',
      pointBackgroundColor: 'rgba(245, 92, 92, 1)',
      pointBorderColor: '#f55c5c',
      pointHoverBackgroundColor: '#f55c5c',
      pointHoverBorderColor: 'rgba(245, 92, 92, 1)'
    },
  ];

  barChartData: Array<any> = [];
  testList: any[];

  formGroup: FormGroup;
  formToggleGroup: FormGroup;
  chartForm: FormGroup;

  constructor(
    public dialog: MatDialog,
    private facultyattendanceService: PrincipalAttendanceService,
    private _formBuilder: FormBuilder,
    private dashboardService: DashboardService,
  ) {
  }

  ngOnInit() {
    // this.dataSource.paginator = this.paginator;
    // this.pagination = this.facultyattendanceService.initializePaginationParams();
    this.userId = localStorage.getItem('userId');
    this.instituteId = localStorage.getItem('instituteId');
    this.departmentId = localStorage.getItem('departmentId');

    //console.log("userid ::",this.userId,"   insti::",this.instituteId,"   depart ::",this.departmentId);

    // this.getInstituteDropdown();
    this.getDepartmentDropdown();
    // this.getFacultyDropdown();
    this.getTodaysEndrosStatus();
    this.getList(1);
    this.principalChart();
    this.getTotalAttendanceList();

    // this.dataSource.sort = this.sort;
    this.formGroup = this._formBuilder.group({
      searchText: ['', []],
      instituteId: ['', []],
      departmentId: ['', []],
      startDate: ['', []],
      endDate: ['', []],
      facultyId: ['', []]
    });

    this.formToggleGroup = this._formBuilder.group({
      endros: [false, []],
      endrosByHod: ['', []]
    });

    this.chartForm = this._formBuilder.group({
      isMonthly: [true, []],
      isWeekly: [false, []]
    });
  }

  search(formValue, formValid) {
    if (formValid) {
      this.getList(1);
    }
  }

  getDepartmentDropdown() {
    this.facultyattendanceService.getDepartmentListByInstitute(this.instituteId)
      .subscribe(res => {
        this.departmentList = res;
      }, err => {
      });
  }

  onDeartmentChange(){
    // console.log("on deartment change ::",this.searchObj.departmentId);
    this.getFacultyDropdown(this.searchObj.departmentId);
  }

  getFacultyDropdown(departmentId){
    this.facultyList = [];
    if (!departmentId) {
      return;
    }
    this.facultyattendanceService.getFacultyByDepartmentId(departmentId)
      .subscribe(res => {
        this.facultyList = res;
      }, err => {
        // this._popupService.sh   owError(err.message);
      });
  }


  pageChanged(pageNo): void {
    pageNo = pageNo + 1;
    this.getList(pageNo);
  };

  getList(page: number) {
    this.loading = true;
    // console.log("page number ::", page);
    var olderPageValue = this.pagination.page;
    this.pagination.page = page - 1;

    let facultyAttendanceRequest = {
      "departmentId": "",
      "instituteId": 0,
      "facultyId": "",
      "startDate": "",
      "endDate": "",
      "searchText": "",
      "page": 0,
      "limit": 10
    }

    if (this.searchObj.departmentId !== "") {
      facultyAttendanceRequest.departmentId = this.searchObj.departmentId;
    } else {
      facultyAttendanceRequest.departmentId = "";
    }

    if (this.searchObj.startDate != "") {
      facultyAttendanceRequest.startDate = this.searchObj.startDate;
    } else {
      facultyAttendanceRequest.startDate = "";
    }

    if (this.searchObj.endDate !== "") {
      facultyAttendanceRequest.endDate = this.searchObj.endDate;
    } else {
      facultyAttendanceRequest.endDate = "";
    }

    if (this.searchObj.facultyId != "") {
      facultyAttendanceRequest.facultyId = this.searchObj.facultyId;
    } else {
      facultyAttendanceRequest.facultyId = "";
    }
    // console.log("institute Id ::" + this.instituteId);
    facultyAttendanceRequest.instituteId = this.instituteId;
    facultyAttendanceRequest.page = this.pagination.page;
    facultyAttendanceRequest.limit = 10;
    this.facultyattendanceService.getFacultyAttendanceList(facultyAttendanceRequest)
      .subscribe(
        res => {
          this.loading= false;
          this.dataSource = res.content;
          // console.log("response size ::"+res.content.length);
          if (res.content.length > 0) {
            // console.log("response ::",res.content);
            this.displayInstituteName = res.content[0].instituteName;
          }
          this.pagination.total = res.totalElements;
          this.pagination.currentPage = page;
          this.pagination.page = page;
          this.pagination.size = 10;
        },
        error => {
          this.loading= false;
          this.pagination.page = olderPageValue;
          // console.log(error);
        });
  }

  onDelete(id) {
    this.facultyattendanceService.deleteFacultyAttendance(id)
      .subscribe(
        data => {
          // console.log("faculty attendance data deleted successfully");
          this.getList(1);
        },
        error => console.log('ERROR: ' + error));
  }


  // Methods for charts

  public chartClicked(e: any): void {
    console.log(e);
  }

  public chartHovered(e: any): void {
    console.log(e);
  }

  public randomize(): void {
    // Only Change 3 values
    const data = [
      Math.round(Math.random() * 100),
      59,
      80,
      Math.random() * 100,
      56,
      Math.random() * 100,
      40
    ];
    const clone = JSON.parse(JSON.stringify(this.barChartData));
    clone[0].data = data;
    this.barChartData = clone;

  }

  // ismonthly($event) {
  //   this.principalChart($event.value);
  // }
  // isWeekly($event) {
  //   this.principalChart($event.value);
  // }
// OLD Method
//   principalChart() {
//     let dashboardRequest = {
//       // "isMonthly": true,
//       // "isLastSevenDay": false,
//       "instituteId": 0
//
//     }
//     // if (value == "isMonthly") {
//     //   dashboardRequest.isMonthly = true;
//     //   dashboardRequest.isLastSevenDay = false;
//     // }
//     // if (value == "isWeekly") {
//     //   dashboardRequest.isMonthly = false;
//     //   dashboardRequest.isLastSevenDay = true;
//     // }
//     dashboardRequest.instituteId = this.instituteId;
//     this.barChartLabels = [];
//     this.dashboardService.getPrincipalChart(dashboardRequest)
//       .subscribe(
//         res => {
//           this.testArray = res;
//           var presentData = [];
//           var absentData = [];
//
//           let newChartData: Array<{ data: Array<any>, label: string }> = [];
//           // this.barChartLabels =['02-nov-2018', '01-nov-2018', '31-oct-2018'];
//
//           var departmentListData = [];
//           this.testArray.forEach(element => {
//             // let x = element.date.split(" ");
//             // if (this.barChartLabels.indexOf(x[0]) == -1) {
//             //   this.barChartLabels.push(x[0]);
//             // }
//             if (element.instituteName) {
//               departmentListData.push(element.instituteName);
//             }
//             if (element.status) {
//               presentData.push(element.count);
//             }
//             if (!element.status) {
//               // newChartData.push({ data: element.count, label: 'Absent'})
//               absentData.push(element.count);
//
//             }
//           });
//
//           this.barChartLabels = Array.from(new Set(departmentListData));
//           newChartData.push({ data: presentData, label: 'Present' }, { data: absentData, label: 'Absent' });
//
//           // newChartData.push({ data: [1,1,2,3,2,3,4], label: 'Present' }, { data: [1,1,4,2,3,4,0], label: 'Absent' });
//           // console.log("bar chart labels ::", this.barChartLabels);
//           // console.log("New Chart Data ::", newChartData);
//           this.barChartData = newChartData;
//           this.isPrincipalChartReady = true;
//
//         },
//         error => console.log(error));
//   }
    principalChart() {
        let dashboardRequest = {
            "instituteId": 0
        }
        dashboardRequest.instituteId = this.instituteId;
        this.barChartLabels = [];
        this.dashboardService.getPrincipalChart(dashboardRequest)
            .subscribe(
                res => {
                    this.testArray = res;
                    var presentData = [];
                    var absentData = [];
                    let newChartData: Array<{ data: Array<any>, label: string, color: string }> = [];
                    var departmentListData = [];
                    this.testArray.forEach(element => {
                        if (element.instituteName) {
                            departmentListData.push(element.instituteName);
                        }
                        presentData.push(element.count);
                        absentData.push(element.absentCount);

                    });

                    this.barChartLabels = Array.from(new Set(departmentListData));
                    newChartData.push({ data: presentData, label: 'Present', color: '#00FF44' }, { data: absentData, label: 'Absent', color: "#FF0002" });
                    this.barChartData = newChartData;
                    this.isPrincipalChartReady = true;

                },
                error => console.log(error));
    }
  onChange(event) {
    // console.log("on change ::", event.checked);
    let endrosRequest = {
      "endrosStatus": event.checked,
      "instituteId": 1,
      "departmentId": 1
    }
    this.facultyattendanceService.updateEndrosStatus(endrosRequest)
      .subscribe(
        data => {
          this.getTodaysEndrosStatus();
        },
        error => console.log('ERROR: ' + error));
  }

  getTodaysEndrosStatus() {
    let endrosRequest = {
      "hodId": this.userId,
      "instituteId": this.instituteId,
      "departmentId": 1
    }
    if (this.departmentId != null) {
      endrosRequest.departmentId = this.departmentId;
    }
    // console.log("request for rndros ::", endrosRequest);
    this.facultyattendanceService.getEndrosStatus(endrosRequest)
      .subscribe(
        data => {
          this.formToggleGroup.controls['endros'].setValue(data);
        },
        error => { console.log('ERROR: ' + error) });

    this.facultyattendanceService.getEndrosStatusByHod(endrosRequest)
      .subscribe(
        data => {
          this.formToggleGroup.controls['endrosByHod'].setValue(data);
        },
        error => { console.log('ERROR: ' + error) });
  }

  // openDialog(program) {
  //   const dialogRef = this.dialog.open(AddInstituteComponent, {
  //     width: '600px',
  //     data: program
  //   });

  //   dialogRef.afterClosed().subscribe(result => {
  //    // this.getList(this.pagination.page);
  //    this.getList();
  //   });
  // }

  changefunctionForStartDate(i) {
    // console.log("i ::", i);
    const momentDate = new Date(i); // Replace event.value with your date value
    const formattedDate = moment(momentDate).format("DD/MM/YYYY");
    // return formattedDate;
    // this.searchObj.startDate = formattedDate;
    this.formGroup.controls.startDate.value.setValue = formattedDate;
    // console.log("format group ::", this.formGroup.controls.startDate.value.setValue = formattedDate);
  }

  changefunctionForEndDate(i) {
    const momentDate = new Date(i); // Replace event.value with your date value
    const formattedDate = moment(momentDate).format("DD/MM/YYYY");

    //this.formGroup.controls['endDate'].setValue(formattedDate);
  }

  getTotalAttendanceList() {
    // let totalStudentPresent ={
    // }
    this.facultyattendanceService.getTotalAttendance()
      .subscribe(
        data => {
          this.totalStudentAttendanceList = data;
        },
        error => { console.log('ERROR: ' + error) });

  }

  displayedColumnsAttend = ['facultyName', 'slot', 'sem', 'totalCount', 'total', 'subjectName'];
  displayedColumns = ['position', 'facultyId', 'attendanceDate', 'departmentName', 'inTime', 'outTime', 'attendance'];
}
