import { Component, OnInit, ViewChild } from '@angular/core';
import { UniversityService } from '../../Services/university.service';
import { MatPaginator, MatSort, MatTableDataSource, MatDialog } from '@angular/material';
import { University } from '../../Model/university-model';
import { AddUniversityComponent } from './add-university/add-university.component';

@Component({
  selector: 'app-university',
  templateUrl: './university.component.html',
  styleUrls: ['./university.component.scss']
})
export class UniversityComponent implements OnInit {
  universityList =[];
  dataSource: MatTableDataSource<University>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    public dialog: MatDialog,
    private universityService: UniversityService
  ) {
  }

  ngOnInit() {
    this.getList();
    // this.dataSource.paginator = this.paginator;
    // this.dataSource.sort = this.sort;
  }

  getList(){

    this.universityService.getUniversityList()
      .subscribe(
        res => {
          // console.log(res);
          // this.universityList = res;
          this.dataSource = res;
        //  this.customer = res as Customer;
        },
        error => {console.log(error);

        }
        );
  }

  onDelete(id){
    this.universityService.deleteUniversity(id)
      .subscribe(
        data => {
          // console.log("university data deleted successfully");
          this.getList();
        },
        error => console.log('ERROR: ' + error));
  }

  openDialog(university) {
    const dialogRef = this.dialog.open(AddUniversityComponent, {
      width: '600px',
      data: university
    });

    dialogRef.afterClosed().subscribe(result => {
     // this.getList(this.pagination.page);
     this.getList();
    });
  }

  displayedColumns = ['id', 'name', 'code', 'link', 'action'];
}
