import { Component, OnInit, Inject } from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { University } from '../../../Model/university-model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UniversityService } from '../../../Services/university.service';

@Component({
  selector: 'app-add-university',
  templateUrl: './add-university.component.html',
  styleUrls: ['./add-university.component.scss']
})
export class AddUniversityComponent implements OnInit {

  isEdit = false;
  formGroup: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<AddUniversityComponent>,
    @Inject(MAT_DIALOG_DATA) public university: University,
    private _formBuilder: FormBuilder,
    private _universityService: UniversityService,
    // private _popupService: PopupService,
  ) {
    if (university && university.id) {
      this.isEdit = true;
    }
   }

  ngOnInit() {
    this.formGroup = this._formBuilder.group({
      name: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(32)]],
      description: ['', []],
      code: ['', [Validators.required]],
      link: ['', []],
      // active: [true, [Validators.required]]
    });
  }

  onClose(): void {
    this.dialogRef.close();
  }

  getNameErrorMessage() {
    let control = this.formGroup.controls.name;
    return control.hasError('required') ? '' :
      (control.hasError('minlength') || control.hasError('maxlength')) ? 'Name must be between 3 and 32' : '';
  }

  save(formValue, formValid) {
    // console.log("form value ::", formValue);
    // console.log("form valid ::", formValid);
    if (formValid) {
      let saveUniversity;
      let universityObj: University = {
        name: this.university.name,
        description: this.university.description,
        code: this.university.code,
        link: this.university.link,
        id: this.university.id
        // active: this.city.active,
        // stateId: Number(this.selectedStateId)
      }

      if (this.university.id) {
        universityObj.id = this.university.id;
        saveUniversity = this._universityService.updateUniversity(universityObj);
      } else {
        saveUniversity = this._universityService.createUniversity(universityObj);
      }
        // console.log("university request ::"+saveUniversity);
      saveUniversity.subscribe(
        res => {
          this.dialogRef.close();
          // console.log("Updated Successfully");
          // this.toastr.success(MESSAGE_SUCCESS.CITY_SAVED);
        },
        err => {
          // console.log("Error occured");
          // this._popupService.showError(err.message);
        });
    }
  }


}
