import { Component, OnInit, ViewChild } from '@angular/core';
import { HodAttendanceService } from '../../Services/hod-attendance.service';
import { MatPaginator, MatSort, MatTableDataSource, MatDialog } from '@angular/material';
import { HodAttendance } from '../../Model/hod-attendance-model';
import { FormBuilder, FormGroup } from '@angular/forms';
import { DashboardService } from '../../Services/dashboard.service';
import { ArrayDataSource } from '@angular/cdk/collections';
//import { Institute } from './institute-model';

@Component({
  selector: 'app-hod-attendance',
  templateUrl: './hod-attendance.component.html',
  styleUrls: ['./hod-attendance.component.scss']
})
export class HodAttendanceComponent implements OnInit {
  universityList = [];
  dataSource: MatTableDataSource<HodAttendance>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  userId;
  instituteId;
  departmentId;

  searchObj = {
    searchText: "",
    instituteId: "",
    departmentId: "",
    startDate: "",
    endDate: "",
    facultyId: ""
  }

  instituteList = [];
  departmentList = [];
  facultyList = [];
  displayInstituteName;
  displayDepartmentName;

  // pagination = {
  //   "page": 0,
  //   "size": 10,
  //   "total": 0,
  //   "currentPage": 0
  // }

  isPrincipalChartReady = false;
  testArray;
  totalStudentAttendanceList;

  barChartOptions = {
    scaleOverride: true,
    scaleStartValue: 0,
    isStacked: true,
    scales: {
      yAxes: [{
        stacked: true,
        display: true,
        ticks: {
          beginAtZero: true,
          steps: 1,
          stepValue: 4,
        }
      }],
      xAxes: [{
        stacked: true,
        display: true,

      }]
    },
    title: {
      display: true,
      // text: 'Weekly Attendance'
    }
  };
  public barChartLabels: string[] = [];
  public barChartType: string = 'bar';
  public barChartLegend: boolean = true;
  // public barchartColor : string[] =["#ff1212", "#12ff12"];
  public barchartColor: Array<any> = [
    {
      // green
      backgroundColor: 'rgba(119, 224, 108, 1)',
      borderColor: 'rgba(119, 224, 108, 1)',
      pointBackgroundColor: 'rgba(119, 224, 108, 1)',
      pointBorderColor: '#126312',
      pointHoverBackgroundColor: '#126312',
      pointHoverBorderColor: 'rgba(119, 224, 108, 1)'
    },
    {
      // dark red
      backgroundColor: 'rgba(245, 92, 92, 1)',
      borderColor: 'rgba(245, 92, 92, 1)',
      pointBackgroundColor: 'rgba(245, 92, 92, 1)',
      pointBorderColor: '#f55c5c',
      pointHoverBackgroundColor: '#f55c5c',
      pointHoverBorderColor: 'rgba(245, 92, 92, 1)'
    },
  ];

  barChartData: Array<any> = [];
  testList: any[];

  formGroup: FormGroup;
  formToggleGroup: FormGroup;
  chartForm: FormGroup;

  constructor(
    public dialog: MatDialog,
    private facultyattendanceService: HodAttendanceService,
    private _formBuilder: FormBuilder,
    private dashboardService: DashboardService,

  ) {
    this.formGroup = this._formBuilder.group({
      searchText: ['', []],
      instituteId: ['', []],
      departmentId: ['', []],
      startDate: ['', []],
      endDate: ['', []],
      facultyId: ['', []]
    });
    this.formToggleGroup = this._formBuilder.group({
      endros: [false, []]
    });


  }

  ngOnInit() {

    this.userId = localStorage.getItem('userId');
    this.instituteId = localStorage.getItem('instituteId');
    this.departmentId = localStorage.getItem('departmentId');

    this.chartForm = this._formBuilder.group({
      isMonthly: [false, []],
      isWeekly: [true, []]
    });

    this.getInstituteDropdown();
    this.getDepartmentDropdown();
    this.getTodaysEndrosStatus();
    this.getFacultyDropdown();
    this.hodChart("isWeekly");
    this.getTotalAttendanceList();
    this.getList(1);

  }

  search(formValue, formValid) {
    if (formValid) {
      this.getList(1);
    }
  }

  getInstituteName(instituteId) {
    let institute = this.instituteList.find(obj => { return obj.id = instituteId });
    if (institute)
      return institute.name;
  }

  getInstituteDropdown() {
    this.facultyattendanceService.getInstituteList()
      .subscribe(res => {
        // console.log("instituteList ::", res);
        this.instituteList = res;
      }, err => {
        //this._popupService.showError(err.message);
      });
  }
  getFacultyDropdown() {
    this.facultyattendanceService.getFacultyListByDepartment(this.departmentId)
      .subscribe(res => {
        // console.log("Faculty List ::", res);
        this.facultyList = res;
      }, err => {
        //this._popupService.showError(err.message);
      });
  }

  onInstituteChange() {
    this.searchObj.departmentId = '0';
    this.departmentList = [];
    if (this.searchObj.instituteId == '0') {
      return;
    }
    //    this.getDepartmentDropdown(this.searchObj.instituteId);
  }

  // getDepartmentDropdown(institute) {
  //   this.departmentList = [];
  //   if (!instituteId) {
  //     return;
  //   }
  //   this._stateService.dropdown(countryId)
  //     .subscribe(res => {
  //       this.stateList = res;
  //     }, err => {
  //       this._popupService.showError(err.message);
  //     });
  // }

  getDepartmentDropdown() {
    this.facultyattendanceService.getDepartmentList()
      .subscribe(res => {
        this.departmentList = res;
      }, err => {
        //this._popupService.showError(err.message);
      });
  }


  pageChanged(pageNo): void {
    pageNo = pageNo + 1;
    this.getList(pageNo);
  };

  getList(page: number) {
    // var olderPageValue = this.pagination.page;
    // this.pagination.page = page;
    // this.pagination.page = page -1;

    let facultyAttendanceRequest = {
      "departmentId": "",
      "instituteId": "",
      "facultyId": "",
      "startDate": "",
      "endDate": "",
      "searchText": "",
      "page": 0,
      "limit": 100
    }

    if (this.searchObj.startDate != "") {
      facultyAttendanceRequest.startDate = this.searchObj.startDate;
    } else {
      facultyAttendanceRequest.startDate = "";
    }

    if (this.searchObj.endDate !== "") {
      facultyAttendanceRequest.endDate = this.searchObj.endDate;
    } else {
      facultyAttendanceRequest.endDate = "";
    }

    if (this.searchObj.facultyId != "") {
      facultyAttendanceRequest.facultyId = this.searchObj.facultyId;
    } else {
      facultyAttendanceRequest.facultyId = "";
    }

    facultyAttendanceRequest.departmentId = this.departmentId;
    facultyAttendanceRequest.instituteId = this.instituteId;
    facultyAttendanceRequest.page = 0;
    // facultyAttendanceRequest.limit = 10;

    // console.log("request send ::", facultyAttendanceRequest);
    this.facultyattendanceService.getFacultyAttendanceList(facultyAttendanceRequest)
      .subscribe(
        res => {
          // console.log("Faculty Attendance List ::", res);
          // this.dataSource = res.content;
          this.dataSource = new MatTableDataSource(res.content);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;

          if (res.content.length > 0) {
            this.displayInstituteName = res.content[0].instituteName;
            this.displayDepartmentName = res.content[0].departmentName;
          }

          // this.pagination.total = res.totalElements;
          // this.pagination.currentPage = page;
          // this.pagination.page = page;
          // this.pagination.size = 10;
        },
        error => {
          // this.pagination.page = olderPageValue;
          console.log(error);
        });
  }
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
    // if (this.dataSource.paginator) {
    //     this.dataSource.paginator.firstPage();
    // }
  }

  onDelete(id) {
    this.facultyattendanceService.deleteFacultyAttendance(id)
      .subscribe(
        data => {
          // console.log("faculty attendance data deleted successfully");
          this.getList(1);
        },
        error => console.log('ERROR: ' + error));
  }

  onChange(event) {
    // console.log("on change ::", event.checked);
    let endrosRequest = {
      "endrosStatus": event.checked,
      "instituteId": 1,
      "departmentId": 1
    }
    this.facultyattendanceService.updateEndrosStatus(endrosRequest)
      .subscribe(
        data => {
          this.getTodaysEndrosStatus();
        },
        error => console.log('ERROR: ' + error));
  }

  getTodaysEndrosStatus() {
    let endrosRequest = {
      "hodId": 4,
      "instituteId": 1,
      "departmentId": 1
    }
    this.facultyattendanceService.getEndrosStatus(endrosRequest)
      .subscribe(
        data => {
          this.formToggleGroup.controls['endros'].setValue(data);
          // console.log("form group ::", this.formToggleGroup.controls.endros);
        },
        error => console.log('ERROR: ' + error));
  }

  getTotalAttendanceList() {
    this.facultyattendanceService.getTotalAttendance()
      .subscribe(
        data => {
          // console.log("response ::", data);
          this.totalStudentAttendanceList = data;
        },
        error => { console.log('ERROR: ' + error) });

  }

  hodChart(value) {
    let dashboardRequest = {
      "isMonthly": false,
      "isLastSevenDay": true,
      "instituteId": 0,
      "departmentId": 0

    }
    if (value == "isMonthly") {
      dashboardRequest.isMonthly = true;
      dashboardRequest.isLastSevenDay = false;
    }
    if (value == "isWeekly") {
      dashboardRequest.isMonthly = false;
      dashboardRequest.isLastSevenDay = true;
    }
    dashboardRequest.instituteId = this.instituteId;
    dashboardRequest.departmentId = this.departmentId;
    this.barChartLabels = [];
    // console.log("dashboard request ::", dashboardRequest);
    this.dashboardService.getHodChart(dashboardRequest)
      .subscribe(
        res => {
          // console.log("response for graph ::", res);
          this.testArray = res;
          var presentData = [];
          var absentData = [];
          let newChartData: Array<{ data: Array<any>, label: string }> = [];

          this.testArray.forEach(element => {
             let x = element.date.split(" ")[0];
            // if (this.barChartLabels.indexOf(x[0]) == -1) {
              this.barChartLabels.push(x);
            // }
            // if (element.status) {
              presentData.push(element.count);
            // }
            // if (!element.status) {
              absentData.push(element.absentCount);
            // }
          });

          // this.barChartLabels =  Array.from(new Set(departmentListData ));
          newChartData.push({ data: presentData, label: 'Present' }, { data: absentData, label: 'Absent' });
          // newChartData.push({ data: [1,1,2,3,2,3,4], label: 'Present' }, { data: [1,1,4,2,3,4,0], label: 'Absent' });
          this.barChartData = newChartData;
          // console.log("bar chart labels :",this.barChartLabels);
          // console.log("bar chart data :",this.barChartData);
          this.isPrincipalChartReady = true;
        },
        error => console.log(error));
  }





  displayedColumnsAttend = ['facultyName', 'slot', 'sem', 'totalCount', 'total'];
  displayedColumns = ['position', 'facultyId', 'attendanceDate', 'inTime', 'outTime', 'attendance'];
}
