import { Component, OnInit, ViewChild } from '@angular/core';
import { ProgramService } from '../../Services/program.service';
import { MatPaginator, MatSort, MatTableDataSource, MatDialog } from '@angular/material';
import { Program } from '../../Model/program-model';
import { AddProgramComponent } from './add-program/add-program.component';

@Component({
  selector: 'app-program',
  templateUrl: './program.component.html',
  styleUrls: ['./program.component.scss']
})
export class ProgramComponent implements OnInit {
  programList =[];
  dataSource: MatTableDataSource<Program>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    public dialog: MatDialog,
    private programService: ProgramService
  ) {
  }

  ngOnInit() {
    this.getList();
    // this.dataSource.paginator = this.paginator;
    // this.dataSource.sort = this.sort;
  }

  getList(){

    this.programService.getProgramList()
      .subscribe(
        res => {
          // console.log(res);
          // this.programList = res;
          this.dataSource = res;
        //  this.customer = res as Customer;
        },
        error => console.log(error));
  }

  onDelete(id){
    this.programService.deleteProgram(id)
      .subscribe(
        data => {
          // console.log("program data deleted successfully");
          this.getList();
        },
        error => console.log('ERROR: ' + error));
  }

  openDialog(program) {
    const dialogRef = this.dialog.open(AddProgramComponent, {
      width: '600px',
      data: program
    });

    dialogRef.afterClosed().subscribe(result => {
     // this.getList(this.pagination.page);
     this.getList();
    });
  }

  displayedColumns = ['id', 'name', 'code', 'link', 'action'];
}
