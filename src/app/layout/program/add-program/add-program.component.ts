import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Program } from '../../../Model/program-model';
import { ProgramService } from '../../../Services/program.service';

@Component({
  selector: 'app-add-program',
  templateUrl: './add-program.component.html',
  styleUrls: ['./add-program.component.scss']
})
export class AddProgramComponent implements OnInit {

  isEdit = false;
  formGroup: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<AddProgramComponent>,
    @Inject(MAT_DIALOG_DATA) public program: Program,
    private _formBuilder: FormBuilder,
    private _programService: ProgramService,
    // private _popupService: PopupService,
  ) {
    if (program && program.id) {
      this.isEdit = true;
    }
   }

  ngOnInit() {
    this.formGroup = this._formBuilder.group({
      name: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(32)]],
      description: ['', []],
      code: ['', [Validators.required]],
      link: ['', []],
      // active: [true, [Validators.required]]
    });
  }

  onClose(): void {
    this.dialogRef.close();
  }

  getNameErrorMessage() {
    let control = this.formGroup.controls.name;
    return control.hasError('required') ? '' :
      (control.hasError('minlength') || control.hasError('maxlength')) ? 'Name must be between 3 and 32' : '';
  }

  save(formValue, formValid) {
    // console.log("form value ::", formValue);
    // console.log("form valid ::", formValid);
    if (formValid) {
      let saveProgram;
      let programObj: Program = {
        name: this.program.name,
        description: this.program.description,
        code: this.program.code,
        link: this.program.link,
        id: this.program.id
        // active: this.city.active,
        // stateId: Number(this.selectedStateId)
      }

      if (this.program.id) {
        programObj.id = this.program.id;
        saveProgram = this._programService.updateProgram(programObj);
      } else {
        saveProgram = this._programService.createProgram(programObj);
      }
        // console.log("program request ::"+saveProgram);
      saveProgram.subscribe(
        res => {
          this.dialogRef.close();
          // console.log("Updated Successfully");
          // this.toastr.success(MESSAGE_SUCCESS.CITY_SAVED);
        },
        err => {
          // console.log("Error occured");
          // this._popupService.showError(err.message);
        });
    }
  }


}
