import { Component, OnInit, ViewChild } from '@angular/core';
import { TimeTableService } from '../../Services/time-table.service';
import { MatPaginator, MatSort, MatTableDataSource, MatDialog } from '@angular/material';
import { TimeTable } from '../../Model/time-table-model';
import { FormBuilder, FormGroup } from '@angular/forms';

export interface Days {
  id: number;
  name: string;
}


@Component({
  selector: 'app-time-table',
  templateUrl: './time-table.component.html',
  styleUrls: ['./time-table.component.scss']
})
export class TimeTableComponent implements OnInit {
  facultyActivityList = [];
  dataSource: MatTableDataSource<TimeTable>;

  searchObj = {
    dayId:"",
    facultyId: ""
  }

  dayList: Days[] = [
    {id:1  , name: 'Monday'},
    {id:2  , name: 'Tuesday'},
    {id:3  , name: 'Wednesday'},
    {id:4  , name: 'Thrusday'},
    {id:5  , name: 'Friday'},
    {id:6  , name: 'Saturday'},
  ];

  isPrincipalChartReady = false;
  facultyList = [];
  formGroup: FormGroup;
  userRole;
  instituteId;
  departmentId;
  userId;
  daySelected;
  isFaculty = false;

  constructor(
    public dialog: MatDialog,
    private facultyActivityService: TimeTableService,
    private _formBuilder: FormBuilder,
  ) {
    this.formGroup = this._formBuilder.group({
      facultyId: ['', []],
      dayId: ['',[]]
    });
  }

  ngOnInit() {
    this.userRole = localStorage.getItem('userRole');
    this.userId = localStorage.getItem('userId');
    if (this.userRole == 1) {
      this.isFaculty = true;
      this.getList();
    }
    // console.log("is faculty ::", this.isFaculty);
    this.instituteId = localStorage.getItem('instituteId');
    this.departmentId = localStorage.getItem('departmentId');

  }

  search(formValue, formValid) {
    if (formValid) {
      this.getList();
    }
  }

  getList() {
    var d = new Date();

    let facultyActivityRequest = {
      "facultyId": "",
      "dayId": ""
    }

    if (this.userRole == 1) {
      facultyActivityRequest.facultyId = this.userId;
    }

    if (this.searchObj.dayId !== "") {
      facultyActivityRequest.dayId = this.searchObj.dayId;
    } else {
      facultyActivityRequest.dayId = ""+ d.getDay()+"";
    }


    this.facultyActivityService.timeTable(facultyActivityRequest)
      .subscribe(
        res => {

          // console.log(res);
          if (res && res.length) {
              this.dataSource = res;
              this.daySelected= res[0].day;
          }
        },
        error => {
          console.log(error);
        });
  }

  displayedColumns = ['position', 'slotStart', 'endSlot', 'sem', 'labType', 'subjectCode', 'subjectName'];
}
