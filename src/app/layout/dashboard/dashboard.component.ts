import { Component, OnInit } from '@angular/core';
import { DashboardService } from '../../Services/dashboard.service';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
    displayedColumns = ['position', 'name', 'weight', 'symbol'];
    places: Array<any> = [];
    isPrincipalChartReady = false;
    isHodChartReady = false;
    testArray;

    applyFilter(filterValue: string) {
        filterValue = filterValue.trim(); // Remove whitespace
        filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    }

    // bar chart
    public barChartOptions: any = {
        scaleShowVerticalLines: false,
        responsive: true,
        isStacked: true,
        height: 50,
        width: 300
    };
    //public barChartLabels: string[] = ['02-nov-2018', '01-nov-2018', '31-oct-2018'];
    public barChartLabels: string[] = [];
    public hodBarChartLabels: string[] = [];
    public barChartType: string = 'bar';
    public barChartLegend: boolean = true;

    // public barChartData: any[] = [
    //     { data: [13, 15, 12], label: 'Present' },
    //     { data: [2, 0, 3], label: 'Absent' }
    // ];

    barChartData: Array<any> = [];
    hodBarChartData: Array<any> = [];

    testList: any[];

    constructor(
        private dashboardService: DashboardService,
    ) {
    }

    // events
    public chartClicked(e: any): void {
        // console.log(e);
    }

    public chartHovered(e: any): void {
        // console.log(e);
    }


    public randomize(): void {
        // Only Change 3 values
        const data = [
            Math.round(Math.random() * 100),
            59,
            80,
            Math.random() * 100,
            56,
            Math.random() * 100,
            40
        ];
        const clone = JSON.parse(JSON.stringify(this.barChartData));
        clone[0].data = data;
        this.barChartData = clone;

    }

    ngOnInit() {
        this.principalChart();

        // this.hodChart();
    }
    principalChart() {
        let dashboardRequest = {
            "isMonthly": true,
            "isLastSevenDay": false
        }
        this.dashboardService.getPrincipalChart(dashboardRequest)
            .subscribe(
                res => {
                    this.testArray = res;
                    var presentData = [];
                    var absentData = [];

                    let newChartData: Array<{ data: Array<any>, label: string }> = [];
                    // this.barChartLabels =['02-nov-2018', '01-nov-2018', '31-oct-2018'];
                    this.testArray.forEach(element => {
                        if (this.barChartLabels.indexOf(element.date) == -1) {
                            this.barChartLabels.push(element.date);
                        }
                        if (element.status) {
                            presentData.push(element.count);
                            // newChartData[0].data.push(element.count) ;
                            // newChartData[0].label = 'Present';

                        }
                        if (!element.status) {
                            // newChartData.push({ data: element.count, label: 'Absent'})
                            absentData.push(element.count);

                        }
                    });
                    console.log("Present data :", presentData, "Absent data  ::", absentData);
                    newChartData.push({ data: presentData, label: 'Present' }, { data: absentData, label: 'Absent' });

                    //  newChartData.push({ data: [1,1,2], label: 'Present' }, { data: [1,1,0], label: 'Absent' });
                    console.log("New Chart Data ::", newChartData);
                    this.barChartData = newChartData;
                    this.isPrincipalChartReady = true;

                },
                error => console.log(error));
    }

}
