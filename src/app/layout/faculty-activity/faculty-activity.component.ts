import { Component, OnInit, ViewChild } from '@angular/core';
import { FacultyActivityService } from '../../Services/faculty-activity.service';
import { MatPaginator, MatSort, MatTableDataSource, MatDialog } from '@angular/material';
import { FacultyActivity } from '../../Model/faculty-activity-model';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-faculty-activity',
  templateUrl: './faculty-activity.component.html',
  styleUrls: ['./faculty-activity.component.scss']
})
export class FacultyActivityComponent implements OnInit {
  facultyActivityList = [];
  dataSource: MatTableDataSource<FacultyActivity>;

  searchObj = {
    facultyId: ""
  }

  isPrincipalChartReady = false;
  facultyList = [];
  formGroup: FormGroup;
  userRole;
  instituteId;
  departmentId;
  userId;
  totalHours;
  isFaculty =false;

  // Pie
  public pieChartLabels: string[] = [];
  public pieChartData: number[] = [];
  public pieChartType: string = 'pie';
  public pieChartOptions: any = {
    scaleOverride: true,
    responsive: true
  };

  public pieChartLegend: boolean = true;
  public pieChartColors :Array<any>  = ["#FF5733","#8AFF33","#33FFE9","#334CFF","#FF33B2"];

  public barchartColor: Array<any> = [
    {
      // grey
      backgroundColor: 'rgba(127,191,63,1)',
      borderColor: 'rgba(127,191,63,1)',
      pointBackgroundColor: 'rgba(127,191,63,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(127,191,63,1)'
    },
    {
      // dark grey
      backgroundColor: 'rgba(247, 111, 27,1)',
      borderColor: 'rgba(247, 111, 27,1)',
      pointBackgroundColor: 'rgba(247, 111, 27,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(247, 111, 27,1)'
    },
    {
      // grey
      backgroundColor: 'rgba(148,159,177,0.2)',
      borderColor: 'rgba(148,159,177,1)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },{
      // dark grey
      backgroundColor: 'rgba(27, 45, 247,1)',
      borderColor: 'rgba(27, 45, 247,1)',
      pointBackgroundColor: 'rgba(27, 45, 247,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(27, 45, 247,1)'
    },
  ];



  constructor(
    public dialog: MatDialog,
    private facultyActivityService: FacultyActivityService,
    private _formBuilder: FormBuilder,
  ) {
    this.formGroup = this._formBuilder.group({
      facultyId: ['', []]
    });
  }

  ngOnInit() {
    this.userRole = localStorage.getItem('userRole');
    this.userId = localStorage.getItem('userId');
    if(this.userRole ==1){
      this.isFaculty = true;
      this.getList();
    }
    console.log("is faculty ::",this.isFaculty);
    this.instituteId = localStorage.getItem('instituteId');
    this.departmentId = localStorage.getItem('departmentId');

    this.getFacultyDropdown();
  }

  getFacultyDropdown() {
    console.log("instituteId :: ", this.instituteId);
    this.facultyActivityService.getFacultyListByInstituteId(this.instituteId)
      .subscribe(res => {
        // console.log("Faculty List ::", res);
        this.facultyList = res;
      }, err => {
        //this._popupService.showError(err.message);
      });
  }

  search(formValue, formValid) {
    this.isPrincipalChartReady = false;
    if (formValid) {
      this.getList();
    }
  }

  getList() {
    let facultyActivityRequest = {
      "facultyId": "",
    }
    if (this.searchObj.facultyId !== "") {
      facultyActivityRequest.facultyId = this.searchObj.facultyId;
    }

    if(this.userRole ==1 ){
      facultyActivityRequest.facultyId=  this.userId;
    }

    this.facultyActivityService.getFacultyActivityList(facultyActivityRequest)
      .subscribe(
        res => {
          this.pieChartData = [];
          this.pieChartLabels = [];
          // console.log(res);
          this.dataSource = res;
          var count = 0;

          res.forEach(element => {
            this.pieChartLabels.push(element.message);
            this.pieChartData.push(element.count);
            // console.log("pie chart ::",element.count);
            count= count + element.count;
            // console.log("count ::",count);
          });

          // console.log("labels ::", this.pieChartLabels);
          // console.log("date ::", this.pieChartData);
          // console.log("count total ::",count);
          this.totalHours = count;
          this.isPrincipalChartReady = true;
        },
        error => {
          console.log(error);
        });
  }

  displayedColumns = ['position', 'activity', 'count'];
}
