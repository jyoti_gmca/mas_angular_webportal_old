import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'app-statsmall',
    templateUrl: './statsmall.component.html',
    styleUrls: ['./statsmall.component.scss']
})
export class StatsmallComponent implements OnInit {
    @Input() bgsmClass: string;
    @Input() icon: string;
    @Input() count: number;
    @Input() label: string;
    @Input() data: number;

    constructor() {}

    ngOnInit() {}
}
