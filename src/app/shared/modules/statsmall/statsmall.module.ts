import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StatsmallComponent } from './statsmall.component';
import { MatCardModule } from '@angular/material';
import { MatGridListModule, MatIconModule } from '@angular/material';

@NgModule({
    imports: [CommonModule, MatCardModule, MatGridListModule, MatIconModule],
    declarations: [StatsmallComponent],
    exports: [StatsmallComponent]
})
export class StatsmallModule {}
