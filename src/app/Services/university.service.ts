import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UniversityService {

  private baseUrl = '';
  constructor(private http: HttpClient) {
      this.baseUrl = environment.baseURL;
  }

  getUniversity(id: number): Observable<Object> {
    return this.http.get(`${this.baseUrl}` +`/university` +`/${id}`);
  }

  createUniversity(university: Object): Observable<Object> {
    return this.http.post(`${this.baseUrl}` + `/university/create`, university);
  }

  updateUniversity(university: Object): Observable<Object> {
    return this.http.put(`${this.baseUrl}` + `/university`, university);
  }

  // updateUniversity(id: number, value: any): Observable<Object> {
  //   return this.http.put(`${this.baseUrl}/${id}`, value);
  // }

  deleteUniversity(id: number): Observable<any> {
    return this.http.delete(`${this.baseUrl}`+`/university`+`/${id}`, { responseType: 'text' });
  }

  getUniversityList(): Observable<any> {
    return this.http.get(`${this.baseUrl}`+ `/university/list`);
  }

}
