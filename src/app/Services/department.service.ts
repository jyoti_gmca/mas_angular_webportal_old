import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DepartmentService {

  private baseUrl = '';
  constructor(private http: HttpClient) {
      this.baseUrl = environment.baseURL;
  }

  getDepartment(id: number): Observable<Object> {
    return this.http.get(`${this.baseUrl}` +`/department` +`/${id}`);
  }

  createDepartment(department: Object): Observable<Object> {
    return this.http.post(`${this.baseUrl}` + `/department/create`, department);
  }

  updateDepartment(department: Object): Observable<Object> {
    return this.http.put(`${this.baseUrl}` + `/department`, department);
  }

  deleteDepartment(id: number): Observable<any> {
    return this.http.delete(`${this.baseUrl}`+`/department`+`/${id}`, { responseType: 'text' });
  }

  getDepartmentList(): Observable<any> {
    return this.http.get(`${this.baseUrl}`+ `/department/list`);
  }

}
