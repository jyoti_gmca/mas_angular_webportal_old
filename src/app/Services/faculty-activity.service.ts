import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class FacultyActivityService {
  private baseUrl = '';
  constructor(private http: HttpClient) {
      this.baseUrl = environment.baseURL;
  }

  getFacultyActivityList(facultyActivity: Object): Observable<any> {
    return this.http.post(`${this.baseUrl}` + `/faculty/activity/record`, facultyActivity);
  }

  getFacultyList(): Observable<any> {
    return this.http.get(`${this.baseUrl}` + `/list`);
  }

  getFacultyListByInstituteId(instituteId: number): Observable<any> {
    return this.http.get(`${this.baseUrl}` +`/faculty/list/by/institute` +`/${instituteId}`);
  }

    getShiftListByInstituteId(instituteId: number): Observable<any> {
        return this.http.get(`${this.baseUrl}` +`/shift/institute` +`/${instituteId}`);
    }

  updatedFacultyData(updatedData): Observable<any> {
      return this.http.post(`${this.baseUrl}` + `/faculty/profile/edit`, updatedData);
  }
  createFacultyData(createData): Observable<any> {
      return this.http.post(`${this.baseUrl}` + `/facult`, createData);
  }
  deleteFacultyData(facultyId: number): Observable<any> {
        return this.http.delete(`${this.baseUrl}` + `/faculty` + `/${facultyId}`);
    }

    updatedShiftData(updatedData): Observable<any> {
        return this.http.post(`${this.baseUrl}` + `/facultyShiftMpg`, updatedData);
    }

    getDeparmentByInstituteId(instituteId: number): Observable<any> {
        return this.http.get(`${this.baseUrl}` +`/department/by/institute` +`/${instituteId}`);
    }
}
