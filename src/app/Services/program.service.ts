import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProgramService {

  private baseUrl = '';
  constructor(private http: HttpClient) {
      this.baseUrl = environment.baseURL;
  }

  getProgram(id: number): Observable<Object> {
    return this.http.get(`${this.baseUrl}` +`/program` +`/${id}`);
  }

  createProgram(program: Object): Observable<Object> {
    return this.http.post(`${this.baseUrl}` + `/program/create`, program);
  }

  updateProgram(program: Object): Observable<Object> {
    return this.http.put(`${this.baseUrl}` + `/program`, program);
  }

  deleteProgram(id: number): Observable<any> {
    return this.http.delete(`${this.baseUrl}`+`/program`+`/${id}`, { responseType: 'text' });
  }

  getProgramList(): Observable<any> {
    return this.http.get(`${this.baseUrl}`+ `/program/list`);
  }

}
