import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class LoginService {

  private baseUrl = '';
  constructor(private http: HttpClient) {
    //this.headers = new HttpHeaders().set('content-type', 'application/json');
    this.baseUrl = environment.baseURL;
  }

  getLogin(id: number): Observable<Object> {
    return this.http.get(`${this.baseUrl}` + `/login` + `/${id}`);
  }

  login(loginRequest: Object): Observable<Object> {
    return this.http.post(`${this.baseUrl}` + `/login`, loginRequest);
  }

  getFaculty(userId): Observable<any> {
    return this.http.get(`${this.baseUrl}` + `/faculty/` + userId);
  }

  forgetPassword(forgetPasswordRequest) :Observable<Object> {
    return this.http.post(`${this.baseUrl}` + `/forget/passsword`, forgetPasswordRequest);
  }

}
