import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class FacultyAttendanceService {

  private baseUrl = '';
  constructor(private http: HttpClient) {
      this.baseUrl = environment.baseURL;
  }

  getFacultyAttendance(id: number): Observable<Object> {
    return this.http.get(`${this.baseUrl}` +`/facultyAttendance` +`/${id}`);
  }

  createFacultyAttendance(facultyAttendance: Object): Observable<Object> {
    return this.http.post(`${this.baseUrl}` + `/facultyAttendance/create`, facultyAttendance);
  }

  updateFacultyAttendance(facultyAttendance: Object): Observable<Object> {
    return this.http.put(`${this.baseUrl}` + `/facultyAttendance`, facultyAttendance);
  }

  deleteFacultyAttendance(id: number): Observable<any> {
    return this.http.delete(`${this.baseUrl}`+`/facultyAttendance`+`/${id}`, { responseType: 'text' });
  }

  getInstituteList(): Observable<any> {
    return this.http.get(`${this.baseUrl}`+ `/institute/list`);
  }

  getDepartmentList(): Observable<any> {
    return this.http.get(`${this.baseUrl}`+ `/department/list`);
  }


  // getFacultyAttendanceList(): Observable<any> {
  //   return this.http.get(`${this.baseUrl}`+ `/facultyAttendance/list`);
  // }

  getFacultyAttendanceList(facultyAttendanceRequest: Object): Observable<any> {
    return this.http.post(`${this.baseUrl}`+ `/facultyAttendance/list`, facultyAttendanceRequest);
  }
}
