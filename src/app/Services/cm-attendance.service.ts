import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CmAttendanceService {

  private baseUrl = '';
  constructor(private http: HttpClient) {
      this.baseUrl = environment.baseURL;
  }

  getFacultyAttendance(id: number): Observable<Object> {
    return this.http.get(`${this.baseUrl}` +`/facultyAttendance` +`/${id}`);
  }

  createFacultyAttendance(facultyAttendance: Object): Observable<Object> {
    return this.http.post(`${this.baseUrl}` + `/facultyAttendance/create`, facultyAttendance);
  }

  updateFacultyAttendance(facultyAttendance: Object): Observable<Object> {
    return this.http.put(`${this.baseUrl}` + `/facultyAttendance`, facultyAttendance);
  }

  deleteFacultyAttendance(id: number): Observable<any> {
    return this.http.delete(`${this.baseUrl}`+`/facultyAttendance`+`/${id}`, { responseType: 'text' });
  }

  getInstituteList(instituteTypeId): Observable<any> {
    return this.http.get(`${this.baseUrl}`+ `/institute/list/by/type/`+`${instituteTypeId}`);
  }

  getDepartmentList(): Observable<any> {
    return this.http.get(`${this.baseUrl}`+ `/department/list`);
  }

  getFacultyList(): Observable<any> {
    return this.http.get(`${this.baseUrl}`+ `/list`);
  }

  // getFacultyAttendanceList(): Observable<any> {
  //   return this.http.get(`${this.baseUrl}`+ `/facultyAttendance/list`);
  // }

  getFacultyAttendanceList(facultyAttendanceRequest: Object): Observable<any> {
    return this.http.post(`${this.baseUrl}`+ `/facultyAttendance/list`, facultyAttendanceRequest);
  }

  getTotalRegisteredFaculty() : Observable<any> {
    return this.http.get(`${this.baseUrl}`+ `/total/faculty/register/count`);
  }
  getTotalPresentFaculty() : Observable<any> {
    return this.http.get(`${this.baseUrl}`+ `/total/faculty/present/count`);
  }
  getTotalAbsentFaculty() : Observable<any> {
    return this.http.get(`${this.baseUrl}`+ `/total/faculty/absent/count`);
  }

  getDepartmentByInstitute(instituteId : number) : Observable<any> {
    return this.http.get(`${this.baseUrl}`+ `/department/by/institute` +`/${instituteId}`);
  }

  getFacultyByDepartmentId(departmentId : number) : Observable<any> {
    return this.http.get(`${this.baseUrl}`+ `/faculty/list/by` +`/${departmentId}`);
  }

}
