import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PrincipalAttendanceService {
  initializePaginationParams(): any {
    var pagination ={
      "page":1,
      "limit":5
    }
  }

  private baseUrl = '';
  constructor(private http: HttpClient) {
      this.baseUrl = environment.baseURL;
  }
  getFacultyAttendance(id: number): Observable<Object> {
    return this.http.get(`${this.baseUrl}` + `/facultyAttendance` + `/${id}`);
  }
  getFacultyList(): Observable<any> {
    return this.http.get(`${this.baseUrl}` + `/list`);
  }

  getFacultyListByDepartment(departmentId: number): Observable<any> {
    return this.http.get(`${this.baseUrl}` +`/faculty/list/by` +`/${departmentId}`);
  }

  getFacultyListByInstituteId(instituteId: number): Observable<any> {
    return this.http.get(`${this.baseUrl}` +`/faculty/list` +`/${instituteId}`);
  }

  createFacultyAttendance(facultyAttendance: Object): Observable<Object> {
    return this.http.post(`${this.baseUrl}` + `/facultyAttendance/create`, facultyAttendance);
  }

  updateFacultyAttendance(facultyAttendance: Object): Observable<Object> {
    return this.http.put(`${this.baseUrl}` + `/facultyAttendance`, facultyAttendance);
  }

  deleteFacultyAttendance(id: number): Observable<any> {
    return this.http.delete(`${this.baseUrl}` + `/facultyAttendance` + `/${id}`, { responseType: 'text' });
  }

  getInstituteList(): Observable<any> {
    return this.http.get(`${this.baseUrl}` + `/institute/list`);
  }

  getDepartmentList(): Observable<any> {
    return this.http.get(`${this.baseUrl}` + `/department/list`);
  }

  getDepartmentListByInstitute(instituteId: number): Observable<any> {
    return this.http.get(`${this.baseUrl}` +`/department/by/institute` +`/${instituteId}`);
  }


  // getFacultyAttendanceList(): Observable<any> {
  //   return this.http.get(`${this.baseUrl}`+ `/facultyAttendance/list`);
  // }

  getFacultyAttendanceList(facultyAttendanceRequest: Object): Observable<any> {
    return this.http.post(`${this.baseUrl}` + `/facultyAttendance/list`, facultyAttendanceRequest);
  }

  list(paginationModel) {
    paginationModel.page = paginationModel.page - 1;
    return this.http.post(`${this.baseUrl}` + `/facultyAttendance/list`, paginationModel);
  }

  updateEndrosStatus(endrosRequest: Object): Observable<any> {
    return this.http.post(`${this.baseUrl}` + `/daily/endros/principal`, endrosRequest);
  }

  getEndrosStatus(endrosRequest: Object): Observable<any> {
    return this.http.post(`${this.baseUrl}` + `/daily/endros/status/principal`, endrosRequest);
  }

  getEndrosStatusByHod(endrosRequest: Object): Observable<any> {
    return this.http.post(`${this.baseUrl}` + `/daily/endros/status/hod`, endrosRequest);
  }

  getTotalAttendance(): Observable<any> {
    return this.http.get(`${this.baseUrl}` + `/student/attendance/total/count`);
  }

  getFacultyByDepartmentId(departmentId : number) : Observable<any> {
    return this.http.get(`${this.baseUrl}`+ `/faculty/list/by` +`/${departmentId}`);
  }


}
