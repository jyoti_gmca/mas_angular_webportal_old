import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class InstituteService {

  private baseUrl = '';
  constructor(private http: HttpClient) {
      this.baseUrl = environment.baseURL;
  }

  getInstitute(id: number): Observable<Object> {
    return this.http.get(`${this.baseUrl}` +`/institute` +`/${id}`);
  }

  createInstitute(institute: Object): Observable<Object> {
    return this.http.post(`${this.baseUrl}` + `/institute/create`, institute);
  }

  updateInstitute(institute: Object): Observable<Object> {
    return this.http.put(`${this.baseUrl}` + `/institute`, institute);
  }

  deleteInstitute(id: number): Observable<any> {
    return this.http.delete(`${this.baseUrl}`+`/institute`+`/${id}`, { responseType: 'text' });
  }

  getInstituteList(): Observable<any> {
    return this.http.get(`${this.baseUrl}`+ `/institute/list`);
  }

}
