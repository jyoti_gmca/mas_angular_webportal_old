import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class StudentFeedbackService {

  private baseUrl = '';
  constructor(private http: HttpClient) {
      this.baseUrl = environment.baseURL;
  }

  // getStudentFeedback(id: number): Observable<Object> {
  //   return this.http.get(`${this.baseUrl}` +`/studentFeedback` +`/${id}`);
  // }

  getStudentFeedbackList(studentFeedBack: Object): Observable<any> {
    return this.http.post(`${this.baseUrl}`+ `/student/feedback/list`,studentFeedBack);
  }

  getFacultyListByInstituteId(instituteId: number): Observable<any> {
    return this.http.get(`${this.baseUrl}` +`/faculty/list/by/institute` +`/${instituteId}`);
  }

}
