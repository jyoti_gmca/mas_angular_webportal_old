import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TimeTableService {
  private baseUrl = '';
  constructor(private http: HttpClient) {
      this.baseUrl = environment.baseURL;
  }

  timeTable(timeTableRequest: Object): Observable<any> {
    return this.http.post(`${this.baseUrl}` + `/timetable/list`, timeTableRequest);
  }

  getFacultyList(): Observable<any> {
    return this.http.get(`${this.baseUrl}` + `/list`);
  }

  getFacultyListByInstituteId(instituteId: number): Observable<any> {
    return this.http.get(`${this.baseUrl}` +`/faculty/list/by/institute` +`/${instituteId}`);
  }

}
