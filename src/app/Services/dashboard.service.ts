import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  private baseUrl = '';
  constructor(private http: HttpClient) {
      this.baseUrl = environment.baseURL;
  }

  // getPrincipalChart(): Observable<Object> {
  //   return this.http.get(`${this.baseUrl}` +`/dasboard/principle/status`);
  // }

  getHodChart(dashboardRequest: Object): Observable<Object> {
    return this.http.post(`${this.baseUrl}` +`/dasboard/hod/status`, dashboardRequest);
  }

  getPrincipalChart(dashboardRequest: Object): Observable<Object> {
    return this.http.post(`${this.baseUrl}` + `/dasboard/principle/status`, dashboardRequest);
  }

  getDirectorChart(dashboardRequest: Object): Observable<Object> {
    return this.http.post(`${this.baseUrl}` + `/dasboard/cm/status`, dashboardRequest);
  }


  getDirectorDashboardChart(dashboardRequest: Object): Observable<Object> {
    return this.http.post(`${this.baseUrl}` + `/dasboard/director/status`, dashboardRequest);
  }

  getDirectorChartInstituteWise(dashboardRequest: Object): Observable<Object> {
    return this.http.post(`${this.baseUrl}` + `/dasboard/director/attendance`, dashboardRequest);
  }


}
