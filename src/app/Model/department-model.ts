export class Department {
    id: number;
    name: string;
    code: number;
    link: string;
    description: string;
    // active: boolean;
}