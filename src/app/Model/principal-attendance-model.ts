export class PrincipalAttendance {
    id: number;
    facultyId: number;
    attendanceDate: Date;
    inTime: string;
    outTime: string;
    instituteName: string;
    instituteId : number;
    departmentId : number;
    startDate: string;
    endDate : Date;
    endros: boolean;
    // active: boolean;
}