export class Institute {
    id: number;
    name: string;
    code: number;
    link: string;
    description: string;
    // active: boolean;
}