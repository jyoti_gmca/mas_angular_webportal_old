export class TimeTable {
    id: number;
    name: string;
    code: number;
    link: string;
    description: string;
    facultyId: number;
    // active: boolean;
}