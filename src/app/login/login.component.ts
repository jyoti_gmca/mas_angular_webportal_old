import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoginService } from '../Services/login.service';
import { MatDialog } from '@angular/material';
import { ForgetPasswordComponent } from './forget-password/forget-password.component';
import { environment } from '../../environments/environment';
@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

    public loading = false;
    formGroup: FormGroup;
    loginResponse;
    hide = true;
    constructor(private router: Router,
        private _formBuilder: FormBuilder,
        private loginService: LoginService,
        public dialog: MatDialog,
    ) { }

    ngOnInit() {
        this.formGroup = this._formBuilder.group({
            email: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(32)]],
            password: ['', [Validators.required]],
        });
    }
    onLogin(formValue) {
        this.loading = true;
        // console.log("email in login function::", formValue.email);
        // console.log("password in login function::", formValue.password);

        localStorage.setItem('isLoggedin', 'true');

        let loginRequest = {
            "email": formValue.email,
            "password": formValue.password
        }
        environment.role = 0;
        // console.log("request body ::" + loginRequest);
        this.loginService.login(loginRequest)
            .subscribe(
                res => {
                    // console.log(res);
                    this.loginResponse = res;
                    // console.log("login response role ::" + this.loginResponse.userRole);
                    localStorage.setItem('userRole', this.loginResponse.userRole);
                    localStorage.setItem('userId', this.loginResponse.id);
                    // console.log("user id ::",+this.loginResponse.id);
                    if (this.loginResponse.id != null) {
                        // this.getUserDetails(this.loginResponse.id)
                        // console.log("user id not null::",+this.loginResponse.id);;
                        this.loginService.getFaculty(this.loginResponse.id)
                            .subscribe(res => {
                                this.loading = false;
                                // console.log("faculty response ::", res);
                                localStorage.setItem('instituteId', res.instituteId);
                                localStorage.setItem('departmentId', res.departmentId);
                                if (this.loginResponse.userRole == 1) {
                                    this.router.navigateByUrl('home/faculty-attendance');
                                }
                                else if (this.loginResponse.userRole == 2) {
                                    this.router.navigateByUrl('home/hod-attendance');
                                }
                                else if (this.loginResponse.userRole == 3) {
                                    this.router.navigateByUrl('home/principal-attendance');
                                }
                                else if (this.loginResponse.userRole == 5) {
                                    this.router.navigateByUrl('home/director-attendance');
                                }
                                else if (this.loginResponse.userRole == 6 || this.loginResponse.userRole == 7) {
                                    this.router.navigateByUrl('home/cm-attendance');
                                } else {
                                    this.router.navigateByUrl(`/login`);
                                }
                            }, err => {
                                this.loading = false;
                                //this._popupService.showError(err.message);
                            });
                    }
                },
                error => {
                    this.loading = false;
                    // console.log(error)
                    alert("Please enter correct email and password");
                }
            );
    }

    forgetPassword(){
        const dialogRef = this.dialog.open(ForgetPasswordComponent, {
            width: '600px',
            //data: program
          });

          dialogRef.afterClosed().subscribe(result => {
           this.router.navigate(['/login']);
          });
    }

}

