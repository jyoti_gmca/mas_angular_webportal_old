import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Login } from '../../Model/login-model';
import { LoginService } from '../../Services/login.service';

@Component({
  selector: 'app-forget-password',
  templateUrl: './forget-password.component.html',
  styleUrls: ['./forget-password.component.scss']
})
export class ForgetPasswordComponent implements OnInit {

  isEdit = false;
  formGroup: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<ForgetPasswordComponent>,
    @Inject(MAT_DIALOG_DATA) public login: Login,
    private _formBuilder: FormBuilder,
    private _loginService: LoginService,
    // private _popupService: PopupService,
  ) {
    // if (program && program.id) {
    //   this.isEdit = true;
    // }
   }

  ngOnInit() {
    this.formGroup = this._formBuilder.group({
      email: ['', [Validators.required]],
    });
  }

  onClose(): void {
    this.dialogRef.close();
  }

  save(formValue, formValid) {
    // console.log("form value ::", formValue);
    // console.log("form valid ::", this.formGroup.value['email']);
    if (formValid) {
      let saveProgram;
      let loginObj = {
        "email": ""
      }
      loginObj.email = this.formGroup.value['email'];

      // console.log("forget password email ::"+loginObj.email);
      // console.log("forget password request ::"+loginObj);
        // saveProgram = this._loginService.forgetPassword(loginObj);
        // console.log("program request ::"+saveProgram);
        this._loginService.forgetPassword(loginObj).subscribe(
        res => {
          this.dialogRef.close();
          // console.log("Please Check your new password in your email");
          // this.toastr.success(MESSAGE_SUCCESS.CITY_SAVED);
        },
        err => {
          // console.log("Error occured");
          // this._popupService.showError(err.message);
        });
    }
  }


}
