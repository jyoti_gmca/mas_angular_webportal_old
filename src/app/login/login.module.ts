import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatButtonModule, MatCheckboxModule, MatInputModule, MatDialogModule, MatIconModule, MatCardModule } from '@angular/material';

import { LoginRoutingModule } from './login-routing.module';
import { LoginComponent } from './login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxLoadingModule } from 'ngx-loading';
import { ForgetPasswordComponent } from './forget-password/forget-password.component';

@NgModule({
    entryComponents: [ForgetPasswordComponent],
    imports: [
        CommonModule,
        LoginRoutingModule,
        MatInputModule,
        MatCheckboxModule,
        MatButtonModule,
        MatIconModule,
        MatCardModule,
        FlexLayoutModule,
        FormsModule,
        ReactiveFormsModule,
        MatDialogModule,
        NgxLoadingModule.forRoot({})
    ],
    declarations: [LoginComponent, ForgetPasswordComponent],
    providers: []
})
export class LoginModule {}
